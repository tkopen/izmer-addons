# -*- coding: utf-8 -*-
{
    'name': "NCE Payroll Extended",

    'summary': """""",

    'description': """
    Payroll is the compensation a business must pay to its employees for a set period or on a given date.
    It is usually managed by the accounting or human resources department of a company.
    """,

    'author': "NCE Odoo",
    'website': "http://www.niyel-technologies.com/",
    'category': 'Generic Modules/Human Resources',
    'version': '0.1',
    'depends': ['om_hr_payroll', 'hr_contract', 'project', 'hr_timesheet'],

    'data': [
        'security/ir.model.access.csv',
        'data/data.xml',
        'views/hr_contract_view.xml',
        'views/project_view.xml',
        'views/hr_payroll_view.xml',
        'views/timesheet_view.xml',
        'views/hr_employee.xml',
        'views/hr_loan_view.xml',
    ],
}
