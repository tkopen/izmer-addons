# -*- coding: utf-8 -*-

from odoo import models, fields


class HrContract(models.Model):
    _inherit = 'hr.contract'

    salary_per_hour = fields.Monetary(string="Salary per Hour")
