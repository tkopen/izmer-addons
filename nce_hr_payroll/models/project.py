from odoo import models, fields, api, _
from odoo.exceptions import UserError


class Project(models.Model):
    _inherit = "project.project"

    timesheet_multiplication = fields.Float('Timesheet Multiply', default=1.0)

    @api.constrains('timesheet_multiplication')
    def check_timesheet_multiply(self):
        if self.timesheet_multiplication <= 0.0:
            raise UserError(_('Timesheet Multiplier should be grewater than 0.0 !'))
