from datetime import datetime, timedelta, time

from odoo import _, models, fields, api
from pytz import timezone


class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    def compute_total_paid(self):
        """This compute the total paid amount of Loan.
            """
        for record in self:
            total = 0.0
            for line in record.loan_ids:
                if line.paid:
                    total += line.amount
            record.total_paid = total

    loan_ids = fields.One2many('hr.loan.line', 'payslip_id', string="Loans")
    total_paid = fields.Float(string="Total Loan Amount", compute='compute_total_paid')

    def get_loan(self):
        """This gives the installment lines of an employee where the state is not in paid.
            """
        self.ensure_one()
        loan_list = []
        loan_ids = self.env['hr.loan.line'].search([('employee_id', '=', self.employee_id.id), ('paid', '=', False),
                                                    ('date', '>=', self.date_from), ('date', '<=', self.date_to)])
        if loan_ids:
            loan_ids.write({'paid': True})
        for loan in loan_ids:
            if loan.loan_id.state == 'approve':
                loan_list.append(loan.id)
        self.loan_ids = loan_list
        return loan_list

    def action_payslip_done(self):
        self.ensure_one()
        loan_list = []
        for line in self.loan_ids:
            if line.paid:
                loan_list.append(line.id)
            else:
                line.payslip_id = False
        self.loan_ids = loan_list
        return super(HrPayslip, self).action_payslip_done()

    @api.onchange('employee_id', 'date_from', 'date_to')
    def onchange_employee(self):
        self.ensure_one()
        if (not self.employee_id) or (not self.date_from) or (not self.date_to):
            return
        self.salary_type = self.employee_id.salary_type
        return super().onchange_employee()

    @api.depends('line_ids.total')
    def get_salary_details(self):
        """Method to check salary details on payslip line and update on payslip
            :return:"""
        for salary in self:
            if salary.line_ids:
                net_salary = salary.line_ids.filtered(lambda x: x.salary_rule_id.code == 'NET')
                salary.net_salary = net_salary.total

    salary_type = fields.Selection([('monthly', 'Monthly'), ('timesheet', 'Timesheet')], required=True,
                                   default='monthly')
    net_salary = fields.Float('Net Salary', compute='get_salary_details', store=True)

    @api.model
    def get_duration(self, contract, employee_id, date_from, date_to):
        total_duration = 0.0
        time_sheet_obj = self.env['account.analytic.line']
        time_sheets = []
        employee = self.env['hr.employee'].browse(employee_id)
        if employee:
            time_sheets = time_sheet_obj.search(
                [('user_id', '=', employee.user_id.id), ('date', '>=', date_from), ('date', '<=', date_to)])

        for time_sheet in time_sheets:
            unit_amount = time_sheet.unit_amount
            if time_sheet.project_id:
                unit_amount = time_sheet.unit_amount * time_sheet.project_id.timesheet_multiplication
            total_duration += unit_amount

        tz = timezone(contract.resource_calendar_id.tz)
        working_hours = contract.resource_calendar_id.get_work_hours_count(
            tz.localize(datetime.combine(date_from, time.min)),
            tz.localize(datetime.combine(date_to, time.max)))
        return total_duration, working_hours - total_duration

    @api.model
    def get_worked_day_lines(self, contract_ids, date_from, date_to):
        # @param contract_ids: list of contract id
        # @return: returns a list of dict containing the input that should be applied for
        res = []
        # fill only if the contract as a working schedule linked
        for contract in contract_ids.filtered(
                lambda contract: contract.resource_calendar_id):

            if not contract.resource_calendar_id:
                # fill only if the contract as a working schedule linked
                continue
            day_from = datetime.combine(fields.Date.from_string(date_from), time.min)
            day_to = datetime.combine(fields.Date.from_string(date_to), time.max)
            calendar = contract.resource_calendar_id
            tz = timezone(calendar.tz)
            attendances = {
                'name': _("Normal Working Days paid at 100%"),
                'sequence': 1,
                'code': 'WORK100',
                'number_of_days': 0.0,
                'number_of_hours': 0.0,
                'contract_id': contract.id,
            }
            leaves = {}
            absent = {
                'name': _("Absent Days"),
                'sequence': 2,
                'code': 'ABS',
                'number_of_days': 0.0,
                'number_of_hours': 0.0,
                'contract_id': contract.id,
            }

            timesheet = {
                'name': _("Total Hours Log From Timesheet"),
                'sequence': 1,
                'code': 'TMSHT',
                'number_of_days': 0.0,
                'number_of_hours': 0.0,
                'contract_id': contract.id,
            }
            total_duration, hours_diff = self.get_duration(contract, contract.employee_id.id, date_from, date_to)
            if contract.employee_id.salary_type == 'timesheet':
                timesheet['number_of_hours'] = total_duration

            # contract, contract.employee_id.id, date_from, date_to, context

            day_leave_intervals = contract.employee_id.list_leaves(day_from, day_to,
                                                                   calendar=contract.resource_calendar_id)
            for day, hours, leave in day_leave_intervals:
                holiday = leave.holiday_id
                current_leave_struct = leaves.setdefault(holiday.holiday_status_id, {
                    'name': holiday.holiday_status_id.name or _('Global Leaves'),
                    'sequence': 5,
                    'code': holiday.holiday_status_id.name or 'GLOBAL',
                    'number_of_days': 0.0,
                    'number_of_hours': 0.0,
                    'contract_id': contract.id,
                })
                current_leave_struct['number_of_hours'] += hours
                work_hours = calendar.get_work_hours_count(
                    tz.localize(datetime.combine(day, time.min)),
                    tz.localize(datetime.combine(day, time.max)),
                    compute_leaves=False,
                )
                if work_hours:
                    current_leave_struct['number_of_days'] += hours / work_hours
            if attendances:
                attendances['number_of_days'] = 24.0
            res = [attendances] + [timesheet] + [absent]
            res.extend(leaves.values())
        return res
