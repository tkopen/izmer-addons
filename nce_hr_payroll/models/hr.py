from odoo import api, fields, models


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    salary_type = fields.Selection([('monthly', 'Monthly'), ('timesheet', 'Timesheet')], required=True,
                                   default='monthly')

    @api.model
    def getDuration(self, payslip):
        duration = 0.0
        time_sheet_obj = self.env['account.analytic.line']
        time_sheets = time_sheet_obj.search(
            [('user_id', '=', self.user_id.id), ('date', '>=', payslip.date_from), ('date', '<=', payslip.date_to)])
        for time_sheet in time_sheets:
            duration += time_sheet.unit_amount
        return duration
