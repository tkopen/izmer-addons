# -*- coding: utf-8 -*-
{
    'name': "Currency Converter",
    'summary': """Allow convert currency rates (Inverse)""",
    'description': """""",
    'author': "Niyel Technologies",
    'website': "http://www.niyel-technologies.com/",
    'category': 'Accounting/Accounting',
    'version': '14.0.1.0',
    'depends': ['account'],
    'data': [
        'security/ir.model.access.csv',
        'views/res_currency_view.xml',
        'wizard/currency_rate_wizard.xml',
        'data/cron.xml',
    ],
    'auto_install': True,
}
