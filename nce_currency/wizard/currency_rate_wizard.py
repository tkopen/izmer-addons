from odoo import api, fields, models


class CurrencyRateWizard(models.TransientModel):
	_name = 'res.currency.rate.wizard'
	_description = 'Currency Wizard'

	name = fields.Date(string='Date', required=True,
					   default=lambda self: fields.Date.today())
	company_id = fields.Many2one(comodel_name="res.company", string="Company", required=True,
								 default=lambda self: self.env.company)
	currency_rate_ids = fields.One2many('res.currency.rate.line.wizard', 'wizard_id', 'Currency Line')

	@api.onchange('name')
	def onchange_name(self):
		cur_rates = self.env['res.currency.rate'].search(
			[('name', '=', self.name), ('company_id', '=', self.company_id and self.company_id.id or False)])
		lines = [(5, 0, 0)]
		lines += [(0, 0, {'currency_id': line.currency_id.id, 'rate_tl': line.rate_tl, 'rate': line.rate}) for line in
				  cur_rates]
		self.write({'currency_rate_ids': lines})

	def update_currency_rate(self):
		for record in self:
			if record.currency_rate_ids:
				for p in record.currency_rate_ids:
					rec = self.env['res.currency.rate'].search(
						[('currency_id', '=', p.currency_id.id), ('name', '=', record.name),
						 ('company_id', '=', self.company_id and self.company_id.id or False)])
					if rec:
						rec.update({'name': record.name, 'currency_id': p.currency_id.id, 'rate_tl': p.rate_tl,
									'rate': p.rate})
					else:
						self.env['res.currency.rate'].create({'name': record.name, 'currency_id': p.currency_id.id,
															  'rate_tl': p.rate_tl, 'rate': p.rate})


class NewModule(models.TransientModel):
	_name = 'res.currency.rate.line.wizard'
	_description = 'Currency Rate Line Wizard'

	wizard_id = fields.Many2one('res.currency.rate.wizard', 'Wizard')
	currency_id = fields.Many2one('res.currency', 'Currency', required=True)
	rate_tl = fields.Float(string="Rate (TL)", digits=(12, 6), default=1.0, required=True)
	rate = fields.Float(string="Rate", digits=(12, 6), compute="_compute_currency_rate")

	@api.depends('rate_tl')
	def _compute_currency_rate(self):
		for record in self:
			if record.rate_tl:
				record.rate = 1 / record.rate_tl
