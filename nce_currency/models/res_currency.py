# -*- coding: utf-8 -*-
from datetime import datetime
from urllib.request import urlopen

import xmltodict
from odoo import models, fields, api


class Currency(models.Model):

    _inherit = "res.currency"

    from_rate = fields.Float('Currency Rate', compute='_compute_from_rate', digits=(12, 6))

    @api.depends('rate_ids.rate_tl')
    def _compute_from_rate(self):
        for record in self:
            record.from_rate = record.rate_ids[:1].rate_tl


class CurrencyRate(models.Model):
    _inherit = "res.currency.rate"

    rate_tl = fields.Float(string="Rate (TL)", digits=(12, 6))

    @api.onchange('rate_tl')
    def _compute_currency_rate(self):
        for record in self:
            if record.rate_tl:
                record.rate = 1 / record.rate_tl
            else:
                record.rate = 1.0

    def update_currency_rate(self):
        url = 'http://www.mb.gov.ct.tr/kur/gunluk.xml'
        active_currencies = self.env['res.currency'].search([])
        data_parsed = ''
        cur_rate_obj = self.browse()
        with urlopen(url) as response:
            data = response.read()
            data_parsed = xmltodict.parse(data)
        if not data_parsed:
            return
        cur_dict = {line['Sembol']: line['Doviz_Satis'] for line in
                    data_parsed['KKTCMB_Doviz_Kurlari']['Resmi_Kurlar']['Resmi_Kur']}
        cur_date = datetime.strptime(data_parsed['KKTCMB_Doviz_Kurlari']['Kur_Tarihi'], '%d/%m/%Y')
        currency_rates = self.search([('currency_id', 'in', active_currencies.ids), ('name', '=', cur_date)])
        for currency in active_currencies:
            cur_rate = cur_dict.get(currency.name, False)
            if cur_rate:
                cur_rate = float(cur_rate)
                exist_date = currency_rates.filtered(lambda r: r.currency_id.id == currency.id)
                if exist_date:
                    exist_date.write({'rate_tl': cur_rate, 'rate': 1 / cur_rate})
                else:
                    cur_rate_obj.create({'name': cur_date,
                                         'currency_id': currency.id,
                                         'rate_tl': cur_rate,
                                         'rate': 1 / cur_rate})
        return True
