from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from odoo.tools import float_compare


class PurchaseOrder(models.Model):
	_inherit = 'purchase.order'

	@api.depends('order_line.price_total')
	def _amount_all(self):
		for order in self:
			max_tax_line = max_tax_amount = max_purchase_line = max_purchase_amount = amount_untaxed = amount_tax = 0.0
			for line in order.order_line:
				line._compute_amount()
				amount_untaxed += line.price_subtotal
				amount_tax += line.price_tax
				if line.price_total > max_purchase_amount:
					max_purchase_line = line
					max_purchase_amount = line.price_total
				if line.price_tax > max_tax_amount:
					max_tax_line = line
					max_tax_amount = line.price_tax
			amount_untaxed = order.currency_id.round(amount_untaxed)
			amount_tax = order.currency_id.round(amount_tax)
			amount_total = amount_untaxed + amount_tax
			if float_compare(order.amount_total_wo_discount - order.discount_fixed, order.amount_total,
							 precision_digits=3) != 0:
				amount = order.amount_total_wo_discount - order.discount_fixed
				diff = round(amount_total - amount, 3)
				if max_tax_line:
					max_tax_line.update({'price_tax': max_tax_line.price_tax - diff,
										 'price_total': max_tax_line.price_total - diff})
					amount_tax -= diff
					amount_tax = order.currency_id.round(amount_tax)
					amount_total = amount_tax + amount_untaxed
				else:
					max_purchase_line.update({'price_total': max_purchase_line.price_total - diff,
											  'price_subtotal': max_purchase_line.price_subtotal - diff})
					amount_total -= diff
					amount_untaxed -= diff
			order.update({
				'amount_untaxed': amount_untaxed,
				'amount_tax': amount_tax,
				'amount_total': amount_total,
			})

	@api.depends('order_line.price_total_wo_discount')
	def _amount_all_wo_discount(self):
		for order in self:
			amount_untaxed = amount_tax = 0.0
			for line in order.order_line:
				amount_untaxed += line.price_subtotal_wo_discount
				amount_tax += line.price_tax_wo_discount
			order.update({
				'amount_untaxed_wo_discount': order.currency_id.round(amount_untaxed),
				'amount_tax_wo_discount': order.currency_id.round(amount_tax),
				'amount_total_wo_discount': amount_untaxed + amount_tax,
			})

	discount_type = fields.Selection(
		string="Discount Type", required=True, default='percentage',
		selection=[('percentage', 'Percentage'), ('fixed', 'Fixed')],
		readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}
	)
	# Global discount fields
	discount_fixed = fields.Float(
		string='Discount', digits='Product Price',
		default=0.0, readonly=True,
		states={'draft': [('readonly', False)],
				'sent': [('readonly', False)]}
	)
	discount = fields.Float(
		string='Discount %', digits='Discount', default=0.0, readonly=True,
		states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}
	)
	amount_untaxed_wo_discount = fields.Monetary(
		string='Untaxed Amount WO Discount',
		compute='_amount_all_wo_discount',
		help='Amount Subtotal Without Global Discount',
	)
	amount_tax_wo_discount = fields.Monetary(
		string='Taxes Discount', compute='_amount_all_wo_discount',
		help='Amount Tax Without Global Discount',
	)
	amount_total_wo_discount = fields.Monetary(
		string='Total Discount', compute='_amount_all_wo_discount',
		help='Total Without Global Discount',
	)

	@api.onchange("discount_type")
	def _onchange_discount_type(self):
		if self.discount_type and self.discount_type == 'percentage':
			self.discount_fixed = 0.0
			self.order_line.set_discount_to_zero("fixed")
		elif self.discount_type and self.discount_type == 'fixed':
			self.discount = 0.0
			self.order_line.set_discount_to_zero("percentage")

	@api.constrains('discount_fixed', 'discount')
	def _check_global_discount_values(self):
		for order in self:
			if order.discount_type == "fixed" and (
					order.amount_total < order.discount_fixed or order.discount_fixed < 0):
				raise ValidationError(_("Discount amount must be greater than zero and less than total amount!"))
			if order.discount_type == "percentage" and (100 < order.discount or order.discount < 0):
				raise ValidationError(_("Discount (%) must be between 0 and 100!"))

	def _prepare_invoice(self):
		res = super()._prepare_invoice()
		res.update({
			'discount_type': self.discount_type,
			'discount': self.discount,
			'discount_fixed': self.discount_fixed,
		})
		return res

	def action_rfq_send(self):
		res = super(PurchaseOrder, self).action_rfq_send()
		ctx = dict(res.get('context'))
		if ctx:
			ctx.update(
				dict(custom_layout="nce_mail_templates.mail_notification_paynow"))
			res.update(dict(context=ctx))
		return res


class PurchaseOrderLine(models.Model):
	_inherit = 'purchase.order.line'

	discount = fields.Float(
		string="Discount (%)", digits='Discount', default=0.0
	)
	discount_fixed = fields.Float(
		string='Discount', digits='Product Price', default=0.0
	)
	price_unit_net = fields.Float(
		string='Net Unit Price', compute='_compute_net_price', store=True
	)
	price_unit_net_with_tax = fields.Float(
		string='Net Unit Price (Tax Inc)', compute='_compute_net_price', store=True
	)
	# Global Discount
	discount_global = fields.Float(
		string="Discount Global", digits=(12, 6),
		compute="_compute_global_discount", store=True
	)
	discount_global_fixed = fields.Float(
		string="Discount Global Fixed", digits=(12, 6),
		compute="_compute_global_discount", store=True
	)
	price_subtotal_wo_discount = fields.Monetary(
		compute='_compute_amount_discount',
		string='Subtotal WO Discount',
		help='Subtotal Without Global Discount'
	)
	price_tax_wo_discount = fields.Float(
		compute='_compute_amount_discount',
		string='Total Tax WO Discount',
		help='Tax Without Global Discount'
	)
	price_total_wo_discount = fields.Monetary(
		compute='_compute_amount_discount',
		string='Total WO Discount',
		help='Total Without Global Discount'
	)

	@api.depends('price_total', 'price_subtotal', 'product_uom')
	def _compute_net_price(self):
		for record in self:
			if record.product_uom and record.price_subtotal:
				record.price_unit_net = record.price_subtotal / record.product_uom_qty
				record.price_unit_net_with_tax = record.price_total / record.product_uom_qty
			else:
				record.price_unit_net = 0
				record.price_unit_net_with_tax = 0

	@api.depends('order_id.amount_total_wo_discount', 'order_id.discount', 'order_id.discount_fixed')
	def _compute_global_discount(self):
		for line in self:
			order_id = line.order_id
			if order_id.discount_type and order_id.discount_type == "percentage" and order_id.discount:
				line.discount_global = order_id.discount
				line.discount_global_fixed = 0.0
			elif order_id.discount_type and order_id.discount_type == "fixed" and order_id.discount_fixed:
				line.discount_global_fixed = (line.price_unit - line.discount_fixed) * (
						order_id.discount_fixed / order_id.amount_total_wo_discount)
				line.discount_global = 0.0
			else:
				line.discount_global = 0
				line.discount_global_fixed = 0

	@api.depends('product_qty', 'price_unit', 'taxes_id', 'discount', 'discount_fixed', 'discount_global',
				 'discount_global_fixed')
	def _compute_amount(self):
		for line in self:
			vals = line._prepare_compute_all_values()
			vals['price_unit'] = line._compute_price_reduce(global_discount=True)
			taxes = line.taxes_id.compute_all(
				vals['price_unit'],
				vals['currency_id'],
				vals['product_qty'],
				vals['product'],
				vals['partner'])
			line.update({
				'price_tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
				'price_total': taxes['total_included'],
				'price_subtotal': taxes['total_excluded'],
			})

	@api.depends('product_qty', 'price_unit', 'taxes_id', 'discount', 'discount_fixed')
	def _compute_amount_discount(self):
		for line in self:
			vals = line._prepare_compute_all_values()
			vals['price_unit'] = line._compute_price_reduce()
			taxes = line.taxes_id.compute_all(
				vals['price_unit'],
				vals['currency_id'],
				vals['product_qty'],
				vals['product'],
				vals['partner'])
			line.update({
				'price_tax_wo_discount': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
				'price_total_wo_discount': taxes['total_included'],
				'price_subtotal_wo_discount': taxes['total_excluded'],
			})

	def set_discount_to_zero(self, discount_type):
		for line in self:
			if discount_type == "fixed" and line.discount_fixed:
				line.discount_fixed = 0.0
			elif discount_type == "percentage" and line.discount:
				line.discount = 0.0

	@api.constrains("discount", "discount_fixed")
	def _check_only_one_discount(self):
		for line in self:
			if line.discount and line.discount_fixed:
				raise ValidationError(
					_("You can only set one type of discount per line.")
				)

	def _get_stock_move_price_unit(self):
		self.ensure_one()
		line = self[0]
		order = line.order_id
		price_unit = line._compute_price_reduce(global_discount=True)
		if line.taxes_id:
			price_unit = line.taxes_id.with_context(round=False).compute_all(
				price_unit, currency=line.order_id.currency_id, quantity=1.0, product=line.product_id,
				partner=line.order_id.partner_id
			)['total_void']
		if line.product_uom.id != line.product_id.uom_id.id:
			price_unit *= line.product_uom.factor / line.product_id.uom_id.factor
		if order.currency_id != order.company_id.currency_id:
			price_unit = order.currency_id._convert(
				price_unit, order.company_id.currency_id, self.company_id, self.date_order or fields.Date.today(),
				round=False)
		return price_unit

	def _compute_price_reduce(self, global_discount=False):
		self.ensure_one()
		price_reduce = self.price_unit
		if self.order_id.discount_type == 'percentage':
			price_reduce = price_reduce * (1.0 - self.discount / 100.0)
			if global_discount:
				price_reduce = price_reduce * (1.0 - self.discount_global / 100.0)
		else:
			price_reduce = price_reduce - self.discount_fixed
			if global_discount:
				price_reduce = price_reduce - self.discount_global_fixed
		return price_reduce

	def _prepare_account_move_line(self):
		res = super()._prepare_account_move_line()
		res.update({
			'discount': self.discount,
			'discount_fixed': self.discount_fixed,
			'discount_global': self.discount_global,
			'discount_global_fixed': self.discount_global_fixed
		})
		return res
