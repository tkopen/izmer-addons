# -*- coding: utf-8 -*-
{
	'name': "NCE Purchase Extended",
	'summary': """""",
	'description': """""",
	'author': "NCE Odoo",
	'website': "http://www.niyel-technologies.com/",
	'category': 'Sales/Sales',
	'version': '14.0.1.0',
	'depends': ['purchase', 'purchase_stock', 'nce_account', 'nce_mail_templates'],
	'data': [
		# 'security/ir.model.access.csv',
		'views/purchase_order_view.xml',
		'report/purchase_order_templates.xml',
	],
}
