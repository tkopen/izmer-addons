# -*- coding: utf-8 -*-
{
	'name': "NCE Email Templates",
	'summary': """""",
	'author': "NCE Odoo",
	'website': "http://www.niyel-technologies.com/",
	'category': 'Marketing/Email Marketing',
	'version': '14.0.1.0',
	'depends': ['mail', 'social_media'],
	'data': [
		# 'security/ir.model.access.csv',
		'views/email_template.xml',
	],
}
