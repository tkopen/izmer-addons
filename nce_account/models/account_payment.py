from odoo import _, api, fields, models
from odoo.exceptions import UserError


class AccountPayment(models.Model):
    _name = "account.payment.line"
    _description = "account.payment.line"

    move_id = fields.Many2one(comodel_name="account.move",
                              string="Invoice", required=True)
    payment_id = fields.Many2one(comodel_name="account.payment",
                                 string="Payment", required=True,
                                 readonly=True, ondelete="cascade")
    invoice_date = fields.Date(string='Invoice/Bill Date', related="move_id.invoice_date")
    invoice_date_due = fields.Date(string='Due Date', related="move_id.invoice_date_due")
    currency_id = fields.Many2one(comodel_name="res.currency",
                                  string="Invoice Currency",
                                  related="move_id.currency_id")
    payment_currency_id = fields.Many2one(comodel_name="res.currency",
                                          string="Payment Currency",
                                          related="payment_id.currency_id")
    amount_total = fields.Monetary(string='Total', related="move_id.amount_total")
    amount_residual = fields.Monetary(string='Amount Due')
    amount = fields.Monetary(string='Amount To Pay')
    actual_amount = fields.Monetary(string='Paid Amount', currency_field="payment_currency_id",
                                    compute="compute_actual_amount")
    sequence = fields.Integer(string="Sequence", required=True, default=10)

    @api.depends('amount', 'payment_id.date', 'currency_id')
    def compute_actual_amount(self):
        for line in self:
            if line.amount > 0 and line.currency_id:
                line.actual_amount = line.currency_id._convert(line.amount, line.payment_currency_id,
                                                               line.payment_id.company_id or self.env.company,
                                                               line.payment_id.date or fields.Date.today())
            else:
                line.actual_amount = 0.0

    @api.onchange('move_id')
    def onchange_invoice(self):
        if self.move_id:
            self.write(self.payment_id.prepare_payment_line_values(self.move_id))
        else:
            self.write(self.payment_id.prepare_payment_line_values())


class AccountPayment(models.Model):
    _inherit = "account.payment"

    usd_rate = fields.Float(string="Usd Rate", compute='_compute_currency_rate', digits=(12, 6))
    eur_rate = fields.Float(string="Eur Rate", compute='_compute_currency_rate', digits=(12, 6))
    gbp_rate = fields.Float(string="Gbp Rate", compute='_compute_currency_rate', digits=(12, 6))
    payment_lines = fields.One2many(comodel_name="account.payment.line", inverse_name="payment_id",
                                    string="Payment Lines")
    payment_balance = fields.Monetary(string='Balance (Currency)', compute="compute_payment_balance")
    auto_select = fields.Boolean(string="Auto Select Invoices", default=True)

    def _is_detailed_payment(self):
        return self.payment_lines and self.payment_type in ['inbound', 'outbound']

    def action_post(self):
        res = super().action_post()
        for payment in self:
            if payment._is_detailed_payment():
                if payment.payment_balance < 0:
                    raise UserError(_('You cannot select more value invoices than the payment amount'))
                lines_to_remove = payment.payment_lines.filtered(lambda line: line.amount <= 0)
                for payment_line in (payment.payment_lines - lines_to_remove).sorted('sequence'):
                    moves = payment.move_id + payment_line.mapped('move_id')
                    to_reconcile_move = moves.mapped('line_ids').filtered(
                        lambda line: line.account_id.user_type_id.type in ('receivable', 'payable')
                    )
                    to_reconcile_move.with_context(force_create_exchange_difference=True).reconcile()
                lines_to_remove.unlink()
        return res

    @api.onchange('partner_id', 'payment_type')
    def onchange_partner_id(self):
        move_obj = self.env['account.move']
        if self.partner_id and not self.payment_lines:
            move_type = ''
            if self.payment_type == 'outbound':
                move_type = 'in_invoice'
            elif self.payment_type == 'inbound':
                move_type = 'out_invoice'
            invoices = move_obj.search([('partner_id', '=', self.partner_id.id),
                                        ('state', '=', 'posted'),
                                        ('payment_state', 'in', ('not_paid', 'partial')),
                                        ('move_type', '=', move_type)], order="invoice_date_due")
            line_data = [(0, 0, {
                **self.prepare_payment_line_values(invoice),
                'amount': 0.0,
            }) for invoice in invoices]
            if line_data:
                self.payment_lines = line_data
        if not self.partner_id or any(line.move_id.partner_id != self.partner_id for line in self.payment_lines):
            self.payment_lines.unlink()

    def prepare_payment_line_values(self, invoice):
        self.ensure_one()
        return {
            'move_id': invoice and invoice.id or False,
            'amount_residual': invoice and invoice.amount_residual or 0.0,
        }

    @api.depends('date')
    def _compute_currency_rate(self):
        currency_obj = self.env['res.currency']
        currencies = currency_obj.search([])
        currencies_dict = {record.name: record.id for record in currencies}
        for record in self:
            rates = currencies._get_rates(record.company_id or self.env.company,
                                          fields.Date.to_date(record.date or fields.Date.today()))
            record.usd_rate = 1 / rates.get(currencies_dict.get('USD', False), 1)
            record.eur_rate = 1 / rates.get(currencies_dict.get('EUR', False), 1)
            record.gbp_rate = 1 / rates.get(currencies_dict.get('GBP', False), 1)

    @api.depends('payment_lines.amount', 'payment_lines.actual_amount', 'amount', 'date', 'currency_id')
    def compute_payment_balance(self):
        for payment in self:
            if payment.currency_id and payment.company_id:
                total = 0.0
                for line in payment.payment_lines:
                    total += line.actual_amount
                payment.payment_balance = payment.amount - total
            else:
                payment.payment_balance = 0.0

    def action_recompute_amounts(self):
        self.ensure_one()
        if not self.payment_lines:
            self.onchange_partner_id()
        self.onchange_amount()
        return True

    @api.onchange('amount', 'date', 'currency_id')
    def onchange_amount(self):
        ''' Function to reset/select invoices on the basis of invoice date '''
        if not self.auto_select:
            return
        if self.amount > 0:
            company = self.company_id or self.env.company
            date = self.date or fields.Date.today()
            total_amount = self.amount
            for line in self.payment_lines:
                if total_amount > 0:
                    conv_amount = self.currency_id._convert(total_amount, line.currency_id,
                                                            company, date)
                    if line.amount_residual < conv_amount:
                        line.amount = line.amount_residual
                        if line.currency_id.id == self.currency_id.id:
                            total_amount -= line.amount_residual
                        else:
                            spend_amount = line.currency_id._convert(line.amount_residual, self.currency_id,
                                                                     company, date)
                            total_amount -= spend_amount
                    else:
                        line.amount = self.currency_id._convert(total_amount, line.currency_id, company, date)
                        total_amount = 0
                else:
                    line.amount = 0.0
        if self.amount <= 0:
            for line in self.payment_lines:
                line.amount = 0.0

    def priceText(self):
        self.ensure_one()
        number = self.amount_total
        uclu = (("", "Bir ", "İki ", "Üç ", "Dört ", "Beş ", "Altı ", "Yedi ", "Sekiz ", "Dokuz "),
                ("", "On ", "Yirmi ", "Otuz ", "Kırk ", "Elli ", "Altmış ", "Yetmiş ", "Seksen ", "Doksan "),
                ("", "Yüz ", "İkiyüz ", "Üçyüz ", "Dörtyüz ", "Beşyüz ", "Altıyüz ", "Yediyüz ", "Sekizyüz ",
                 "Dokuzyüz "),
                )
        bloklar = ("", "Bin ", "Milyon ", "Milyar ", "Trilyon ", "Trilyar ")

        integerstr = format(number, ",.2f")
        integerstr = integerstr[0:len(integerstr) - 3]
        numberstr = "%.2f" % number
        decimalstr = numberstr[-2:]
        tl = self.currency_id.currency_unit_label + ' '
        kurus = self.currency_id.currency_subunit_label
        yazi = ""
        decimalyazi = ""
        for i, blok in enumerate(reversed(integerstr.split(","))):
            yazi = bloklar[i] + yazi
            if int(blok) == 1:
                continue
            for j, sayi in enumerate(reversed(blok)):
                yazi = uclu[j][int(sayi)] + yazi

        for k, deci in enumerate(reversed(decimalstr)):
            decimalyazi = uclu[k][int(deci)] + decimalyazi

        if decimalyazi == "":
            kurus = ""

        yazi = yazi + tl + decimalyazi + kurus
        return yazi

    def open_payment_matching_screen(self):
        # Open reconciliation view for customers/suppliers
        move_line_id = False
        for move_line in self.move_id.line_ids:
            if move_line.account_id.reconcile:
                move_line_id = move_line.id
                break
        if not self.partner_id:
            raise UserError(_("Payments without a customer can't be matched"))
        action_context = {'company_ids': [self.company_id.id],
                          'partner_ids': [self.partner_id.commercial_partner_id.id]}
        if self.partner_type == 'customer':
            action_context.update({'mode': 'customers'})
        elif self.partner_type == 'supplier':
            action_context.update({'mode': 'suppliers'})
        if move_line_id:
            action_context.update({'move_line_id': move_line_id})
        return {
            'type': 'ir.actions.client',
            'tag': 'manual_reconciliation_view',
            'context': action_context,
        }
