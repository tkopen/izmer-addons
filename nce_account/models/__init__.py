from . import chart_template
from . import product
from . import partner
from . import account_move
from . import account_payment
from . import currency
from . import tools
