from odoo import api, fields, models


class Partner(models.Model):
	_inherit = 'res.partner'

	def _currencies_get(self):
		currencies_map = {record.name: record for record in self.env['res.currency'].search([])}
		for record in self:
			record.currency_try = currencies_map.get('TRY', False)
			record.currency_usd = currencies_map.get('USD', False)
			record.currency_eur = currencies_map.get('EUR', False)
			record.currency_gbp = currencies_map.get('GBP', False)

	def action_view_partner_invoices(self):
		res = super(Partner, self).action_view_partner_invoices()
		res['context'].update({
			'search_default_partial': 1
		})
		return res

	@api.depends('currency_try', 'currency_eur', 'currency_usd', 'currency_gbp')
	def _credit_debit_get(self):
		domain = [('partner_id', 'in', self.ids), ('state', '=', 'posted'),
				  ('amount_residual', '!=', 0)]
		records = self.env['account.move'].read_group(domain, ['partner_id', 'currency_id', 'amount_residual'],
												  ['partner_id', 'currency_id'], lazy=False)
		record_dict = {(line['partner_id'][0], line['currency_id'][0]): line['amount_residual'] for line in records}
		for record in self:
			record.credit_try = record_dict.get((record.id, record.currency_try.id), 0.0)
			record.credit_usd = record_dict.get((record.id, record.currency_usd.id), 0.0)
			record.credit_euro = record_dict.get((record.id, record.currency_eur.id), 0.0)
			record.credit_gbp = record_dict.get((record.id, record.currency_gbp.id), 0.0)

	currency_try = fields.Many2one('res.currency', string='Currency TRY', compute='_currencies_get')
	currency_eur = fields.Many2one('res.currency', string='Currency EUR', compute='_currencies_get')
	currency_usd = fields.Many2one('res.currency', string='Currency USD', compute='_currencies_get')
	currency_gbp = fields.Many2one('res.currency', string='Currency GBP', compute='_currencies_get')

	credit_try = fields.Monetary(compute='_credit_debit_get', currency_field='currency_try', string="Toplam Alacak(TRY)")
	credit_euro = fields.Monetary(compute='_credit_debit_get', currency_field='currency_eur', string='Toplam Alacak(EUR)')
	credit_usd = fields.Monetary(compute='_credit_debit_get', currency_field='currency_usd', string='Toplam Alacak(USD)')
	credit_gbp = fields.Monetary(compute='_credit_debit_get', currency_field='currency_gbp', string='Toplam Alacak(GBP)')
