from odoo import fields, models


class AccountChartTemplate(models.Model):
	_inherit = "account.chart.template"

	property_account_expense_refund_categ_id = fields.Many2one('account.account.template',
															   string='Category of Expense Refund Account')
	property_account_income_refund_categ_id = fields.Many2one('account.account.template',
															  string='Category of Income Refund Account')
	property_account_expense_refund_id = fields.Many2one('account.account.template',
														 string='Expense Refund Account on Product Template')
	property_account_refund_income_id = fields.Many2one('account.account.template',
														string='Income Refund Account on Product Template')

	def generate_properties(self, acc_template_ref, company):
		res = super(AccountChartTemplate, self).generate_properties(acc_template_ref, company)
		self.ensure_one()
		PropertyObj = self.env['ir.property']
		todo_list = [
			('property_account_expense_refund_categ_id', 'product.category', 'account.account'),
			('property_account_income_refund_categ_id', 'product.category', 'account.account'),
			('property_account_expense_categ_id', 'product.category', 'account.account'),
			('property_account_expense_refund_id', 'product.template', 'account.account'),
			('property_account_refund_income_id', 'product.template', 'account.account'),
		]

		for record in todo_list:
			account = getattr(self, record[0])
			value = account and 'account.account,' + str(self.get_account(record[0])) or False
			if value:
				field = self.env['ir.model.fields'].search(
					[('name', '=', record[0]), ('model', '=', record[1]), ('relation', '=', record[2])], limit=1)
				vals = {
					'name': record[0],
					'company_id': company.id,
					'fields_id': field.id,
					'value': value,
				}
				properties = PropertyObj.search([('name', '=', record[0]), ('company_id', '=', company.id)])
				if properties:
					# the property exist: modify it
					properties.write(vals)
				else:
					# create the property
					PropertyObj.create(vals)
		return res

	def get_account(self, field):
		if field == 'property_account_expense_refund_categ_id':
			return self.property_account_expense_refund_categ_id.id
		elif field == 'property_account_income_refund_categ_id':
			return self.property_account_income_refund_categ_id.id
		elif field == 'property_account_expense_refund_id':
			return self.property_account_expense_refund_id.id
		elif field == 'property_account_refund_income_id':
			return self.property_account_refund_income_id.id
		elif field == "property_account_expense_categ_id":
			return self.property_account_expense_categ_id.id
		return 0
