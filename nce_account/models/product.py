from odoo import fields, models


class ProductCategory(models.Model):
    _inherit = 'product.category'

    property_account_income_refund_categ_id = fields.Many2one('account.account', company_dependent=True,
                                                              string="Income Refund Account",
                                                              domain="['&', ('deprecated', '=', False), ('company_id', '=', current_company_id)]",
                                                              help="This account will be used when validating a customer refund invoice.")
    property_account_expense_refund_categ_id = fields.Many2one('account.account', company_dependent=True,
                                                               string="Expense Refund Account",
                                                               domain="['&', ('deprecated', '=', False), ('company_id', '=', current_company_id)]",
                                                               help="This account will be used when validating a vendor refund invoice.")


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    property_account_refund_income_id = fields.Many2one('account.account', company_dependent=True,
                                                        string="Income Refund Account",
                                                        domain="['&', ('deprecated', '=', False), ('company_id', '=', current_company_id)]",
                                                        help="Keep this field empty to use the default value from the product category.")
    property_account_expense_refund_id = fields.Many2one('account.account', company_dependent=True,
                                                         string="Expense Refund Account",
                                                         domain="['&', ('deprecated', '=', False), ('company_id', '=', current_company_id)]",
                                                         help="Keep this field empty to use the default value from the product category. If anglo-saxon accounting with automated valuation method is configured, the expense account on the product category will be used.")

    def _get_product_accounts(self):
        result = super()._get_product_accounts()
        result.update({
            'income_refund': self.property_account_refund_income_id or self.categ_id.property_account_income_refund_categ_id,
            'expense_refund': self.property_account_expense_refund_id or self.categ_id.property_account_expense_refund_categ_id,
        })
        return result
