def amount_to_words(amount, currency):
	number = amount
	uclu = (("", "Bir ", "İki ", "Üç ", "Dört ", "Beş ", "Altı ", "Yedi ", "Sekiz ", "Dokuz "),
			("", "On ", "Yirmi ", "Otuz ", "Kırk ", "Elli ", "Altmış ", "Yetmiş ", "Seksen ", "Doksan "),
			("", "Yüz ", "İkiyüz ", "Üçyüz ", "Dörtyüz ", "Beşyüz ", "Altıyüz ", "Yediyüz ", "Sekizyüz ",
			 "Dokuzyüz "),
			)
	bloklar = ("", "Bin ", "Milyon ", "Milyar ", "Trilyon ", "Trilyar ")

	integerstr = format(number, ",.2f")
	integerstr = integerstr[0:len(integerstr) - 3]
	numberstr = "%.2f" % number
	decimalstr = numberstr[-2:]
	tl = currency.currency_unit_label + ' '
	kurus = currency.currency_subunit_label
	yazi = ""
	decimalyazi = ""
	for i, blok in enumerate(reversed(integerstr.split(","))):
		yazi = bloklar[i] + yazi
		if int(blok) == 1:
			continue
		for j, sayi in enumerate(reversed(blok)):
			yazi = uclu[j][int(sayi)] + yazi

	for k, deci in enumerate(reversed(decimalstr)):
		decimalyazi = uclu[k][int(deci)] + decimalyazi

	if decimalyazi == "":
		kurus = ""

	yazi = yazi + tl + decimalyazi + kurus
	return yazi