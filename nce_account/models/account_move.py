# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from odoo.tools import float_compare
from .tools import amount_to_words


class AccountMove(models.Model):
    _inherit = 'account.move'

    invoice_date = fields.Date(default=fields.Date.today)
    discount_type = fields.Selection(
        string="Discount Type", required=True,
        selection=[('percentage', 'Percentage'), ('fixed', 'Fixed')],
        default='percentage', readonly=True,
        states={'draft': [('readonly', False)]}
    )
    # Global discount fields
    discount_fixed = fields.Float(
        string='Discount', digits='Product Price',
        default=0, readonly=True,
        states={'draft': [('readonly', False)]}
    )
    discount = fields.Float(
        string='Discount %', digits='Discount',
        default=0, readonly=True,
        states={'draft': [('readonly', False)]}
    )
    discount_amount_subtotal = fields.Float(
        string='Discount Amount Subtotal', compute='_compute_discount_amounts'
    )
    discount_amount_total = fields.Float(
        string='Discount Amount Total', compute='_compute_discount_amounts'
    )
    amount_total_wog_discount = fields.Monetary(
        string='Total without Global Discount',
        compute='_compute_all_custom_amounts',
        help='Total Without Global Discount',
    )
    amount_subtotal_wog_discount = fields.Monetary(
        string='Subtotal without Global Discount',
        compute='_compute_all_custom_amounts',
        help='Amount Subtotal Without Global Discount',
    )
    amount_tax_wog_discount = fields.Monetary(
        string='Tax without Global Discount',
        compute='_compute_all_custom_amounts',
        help='Amount Tax Without Global Discount',
    )
    amount_total_without_discount = fields.Monetary(
        string='Total without Discount',
        compute='_compute_all_custom_amounts',
        help='Total Without Discount',
    )
    amount_subtotal_without_discount = fields.Monetary(
        string='Subtotal without Discount',
        compute='_compute_all_custom_amounts',
        help='Amount Subtotal Without Discount',
    )
    amount_tax_without_discount = fields.Monetary(
        string='Tax without Discount',
        compute='_compute_all_custom_amounts',
        help='Amount Tax Without Discount',
    )

    @api.onchange("discount_type")
    def _onchange_discount_type(self):
        if not self._context.get('change_global_discount', False):
            return {}
        if self.discount_type and self.discount_type == 'percentage':
            self.discount_fixed = 0.0
            self.invoice_line_ids.set_discount_to_zero("fixed")
        elif self.discount_type and self.discount_type == 'fixed':
            self.discount = 0.0
            self.invoice_line_ids.set_discount_to_zero("percentage")
        self._move_autocomplete_invoice_lines_values()

    @api.onchange('discount_fixed', 'discount')
    def _onchange_global_discounts(self):
        if not self._context.get('change_global_discount', False):
            return {}
        if self.discount_type and self.discount_type == 'percentage':
            self.discount_fixed = 0.0
            self.invoice_line_ids.update({'discount_global': self.discount, 'discount_global_fixed': 0.0})
        elif self.discount_type and self.discount_type == 'fixed':
            self.discount = 0.0
            for line in self.invoice_line_ids:
                line.update(
                    {'discount_global_fixed': (line.price_unit - line.discount_fixed) * (
                            self.discount_fixed / self.amount_total_wog_discount),
                     'discount_global': 0.0})
        self._move_autocomplete_invoice_lines_values()

    def _recompute_dynamic_lines(self, recompute_all_taxes=False, recompute_tax_base_amount=False):
        res = super()._recompute_dynamic_lines(recompute_all_taxes=recompute_all_taxes,
                                               recompute_tax_base_amount=recompute_tax_base_amount)

        def _recompute_amounts(base_line, difference):
            is_company_currency = (base_line.currency_id or self.currency_id) == self.company_id.currency_id
            sign = -1 if base_line.balance < 0 else 1
            difference = sign * difference
            base_line.amount_currency -= difference
            if sign < 0:
                if is_company_currency:
                    base_line.credit = abs(base_line.amount_currency)
                else:
                    base_line.credit = abs(base_line.currency_id._convert(base_line.amount_currency,
                                                                          self.company_currency_id, self.company_id,
                                                                          self.date))
            else:
                if is_company_currency:
                    base_line.debit = abs(base_line.amount_currency)
                else:
                    base_line.debit = abs(base_line.currency_id._convert(base_line.amount_currency,
                                                                         self.company_currency_id, self.company_id,
                                                                         self.date))

        if self.discount_fixed:
            if float_compare(self.amount_total_wog_discount - self.discount_fixed, self.amount_total,
                             precision_digits=2) != 0:
                amount = self.amount_total_wog_discount - self.discount_fixed
                diff = round(self.amount_total - amount, 3)
                max_inv_line = max_tax_line = max_partner_line = None
                max_inv_amount = max_tax_amount = max_partner_amount = 0.0
                for line in self.line_ids:
                    company_currency = (line.currency_id or self.currency_id) == self.company_id.currency_id
                    balance = abs(line.balance) if company_currency else abs(line.amount_currency)
                    if not line.exclude_from_invoice_tab:
                        if balance > max_inv_amount:
                            max_inv_line = line
                            max_inv_amount = balance
                    elif line.tax_line_id:
                        if balance > max_tax_amount:
                            max_tax_line = line
                            max_tax_amount = balance
                    elif line.account_id.user_type_id.type in ('receivable', 'payable'):
                        if balance > max_partner_amount:
                            max_partner_line = line
                            max_partner_amount = balance
                if max_tax_line:
                    _recompute_amounts(max_tax_line, diff)
                else:
                    _recompute_amounts(max_inv_line, diff)
                _recompute_amounts(max_partner_line, diff)
        return res

    def priceText(self):
        self.ensure_one()
        if self.partner_id.lang and self.partner_id.lang != 'tr_TR':
            return self.currency_id.amount_to_text(self.amount_total, lang_code=self.partner_id.lang)
        return amount_to_words(self.amount_total, self.currency_id)

    def _recompute_tax_lines(self, recompute_tax_base_amount=False):
        ''' Compute the dynamic tax lines of the journal entry.

        :param lines_map: The line_ids dispatched by type containing:
            * base_lines: The lines having a tax_ids set.
            * tax_lines: The lines having a tax_line_id set.
            * terms_lines: The lines generated by the payment terms of the invoice.
            * rounding_lines: The cash rounding lines of the invoice.
        '''
        self.ensure_one()
        in_draft_mode = self != self._origin

        def _serialize_tax_grouping_key(grouping_dict):
            ''' Serialize the dictionary values to be used in the taxes_map.
            :param grouping_dict: The values returned by '_get_tax_grouping_key_from_tax_line' or '_get_tax_grouping_key_from_base_line'.
            :return: A string representing the values.
            '''
            return '-'.join(str(v) for v in grouping_dict.values())

        def _compute_base_line_taxes(base_line):
            ''' Compute taxes amounts both in company currency / foreign currency as the ratio between
            amount_currency & balance could not be the same as the expected currency rate.
            The 'amount_currency' value will be set on compute_all(...)['taxes'] in multi-currency.
            :param base_line:   The account.move.line owning the taxes.
            :return:            The result of the compute_all method.
            '''
            move = base_line.move_id

            if move.is_invoice(include_receipts=True):
                handle_price_include = True
                sign = -1 if move.is_inbound() else 1
                quantity = base_line.quantity
                is_refund = move.move_type in ('out_refund', 'in_refund')
                price_unit_wo_discount = sign * base_line._compute_price_reduce(global_discount=True)
            else:
                handle_price_include = False
                quantity = 1.0
                tax_type = base_line.tax_ids[0].type_tax_use if base_line.tax_ids else None
                is_refund = (tax_type == 'sale' and base_line.debit) or (tax_type == 'purchase' and base_line.credit)
                price_unit_wo_discount = base_line.balance

            balance_taxes_res = base_line.tax_ids._origin.with_context(
                force_sign=move._get_tax_force_sign()).compute_all(
                price_unit_wo_discount,
                currency=base_line.currency_id,
                quantity=quantity,
                product=base_line.product_id,
                partner=base_line.partner_id,
                is_refund=is_refund,
                handle_price_include=handle_price_include,
            )

            if move.move_type == 'entry':
                repartition_field = is_refund and 'refund_repartition_line_ids' or 'invoice_repartition_line_ids'
                repartition_tags = base_line.tax_ids.flatten_taxes_hierarchy().mapped(repartition_field).filtered(
                    lambda x: x.repartition_type == 'base').tag_ids
                tags_need_inversion = (tax_type == 'sale' and not is_refund) or (tax_type == 'purchase' and is_refund)
                if tags_need_inversion:
                    balance_taxes_res['base_tags'] = base_line._revert_signed_tags(repartition_tags).ids
                    for tax_res in balance_taxes_res['taxes']:
                        tax_res['tag_ids'] = base_line._revert_signed_tags(
                            self.env['account.account.tag'].browse(tax_res['tag_ids'])).ids

            return balance_taxes_res

        taxes_map = {}

        # ==== Add tax lines ====
        to_remove = self.env['account.move.line']
        for line in self.line_ids.filtered('tax_repartition_line_id'):
            grouping_dict = self._get_tax_grouping_key_from_tax_line(line)
            grouping_key = _serialize_tax_grouping_key(grouping_dict)
            if grouping_key in taxes_map:
                # A line with the same key does already exist, we only need one
                # to modify it; we have to drop this one.
                to_remove += line
            else:
                taxes_map[grouping_key] = {
                    'tax_line': line,
                    'amount': 0.0,
                    'tax_base_amount': 0.0,
                    'grouping_dict': False,
                }
        if not recompute_tax_base_amount:
            self.line_ids -= to_remove

        # ==== Mount base lines ====
        for line in self.line_ids.filtered(lambda line: not line.tax_repartition_line_id):
            # Don't call compute_all if there is no tax.
            if not line.tax_ids:
                if not recompute_tax_base_amount:
                    line.tax_tag_ids = [(5, 0, 0)]
                continue

            compute_all_vals = _compute_base_line_taxes(line)

            # Assign tags on base line
            if not recompute_tax_base_amount:
                line.tax_tag_ids = compute_all_vals['base_tags'] or [(5, 0, 0)]

            tax_exigible = True
            for tax_vals in compute_all_vals['taxes']:
                grouping_dict = self._get_tax_grouping_key_from_base_line(line, tax_vals)
                grouping_key = _serialize_tax_grouping_key(grouping_dict)

                tax_repartition_line = self.env['account.tax.repartition.line'].browse(
                    tax_vals['tax_repartition_line_id'])
                tax = tax_repartition_line.invoice_tax_id or tax_repartition_line.refund_tax_id

                if tax.tax_exigibility == 'on_payment':
                    tax_exigible = False

                taxes_map_entry = taxes_map.setdefault(grouping_key, {
                    'tax_line': None,
                    'amount': 0.0,
                    'tax_base_amount': 0.0,
                    'grouping_dict': False,
                })
                taxes_map_entry['amount'] += tax_vals['amount']
                taxes_map_entry['tax_base_amount'] += self._get_base_amount_to_display(tax_vals['base'],
                                                                                       tax_repartition_line,
                                                                                       tax_vals['group'])
                taxes_map_entry['grouping_dict'] = grouping_dict
            if not recompute_tax_base_amount:
                line.tax_exigible = tax_exigible

        # ==== Process taxes_map ====
        for taxes_map_entry in taxes_map.values():
            # The tax line is no longer used in any base lines, drop it.
            if taxes_map_entry['tax_line'] and not taxes_map_entry['grouping_dict']:
                if not recompute_tax_base_amount:
                    self.line_ids -= taxes_map_entry['tax_line']
                continue

            currency = self.env['res.currency'].browse(taxes_map_entry['grouping_dict']['currency_id'])

            # Don't create tax lines with zero balance.
            if currency.is_zero(taxes_map_entry['amount']):
                if taxes_map_entry['tax_line'] and not recompute_tax_base_amount:
                    self.line_ids -= taxes_map_entry['tax_line']
                continue

            # tax_base_amount field is expressed using the company currency.
            tax_base_amount = currency._convert(taxes_map_entry['tax_base_amount'], self.company_currency_id,
                                                self.company_id, self.date or fields.Date.context_today(self))

            # Recompute only the tax_base_amount.
            if recompute_tax_base_amount:
                if taxes_map_entry['tax_line']:
                    taxes_map_entry['tax_line'].tax_base_amount = tax_base_amount
                continue

            balance = currency._convert(
                taxes_map_entry['amount'],
                self.journal_id.company_id.currency_id,
                self.journal_id.company_id,
                self.date or fields.Date.context_today(self),
            )
            to_write_on_line = {
                'amount_currency': taxes_map_entry['amount'],
                'currency_id': taxes_map_entry['grouping_dict']['currency_id'],
                'debit': balance > 0.0 and balance or 0.0,
                'credit': balance < 0.0 and -balance or 0.0,
                'tax_base_amount': tax_base_amount,
            }

            if taxes_map_entry['tax_line']:
                # Update an existing tax line.
                taxes_map_entry['tax_line'].update(to_write_on_line)
            else:
                create_method = in_draft_mode and self.env['account.move.line'].new or self.env[
                    'account.move.line'].create
                tax_repartition_line_id = taxes_map_entry['grouping_dict']['tax_repartition_line_id']
                tax_repartition_line = self.env['account.tax.repartition.line'].browse(tax_repartition_line_id)
                tax = tax_repartition_line.invoice_tax_id or tax_repartition_line.refund_tax_id
                taxes_map_entry['tax_line'] = create_method({
                    **to_write_on_line,
                    'name': tax.name,
                    'move_id': self.id,
                    'partner_id': line.partner_id.id,
                    'company_id': line.company_id.id,
                    'company_currency_id': line.company_currency_id.id,
                    'tax_base_amount': tax_base_amount,
                    'exclude_from_invoice_tab': True,
                    'tax_exigible': tax.tax_exigibility == 'on_invoice',
                    **taxes_map_entry['grouping_dict'],
                })

            if in_draft_mode:
                taxes_map_entry['tax_line'].update(
                    taxes_map_entry['tax_line']._get_fields_onchange_balance(force_computation=True))

    @api.depends('invoice_line_ids.price_total_wog_discount', 'invoice_line_ids.price_total_without_discount')
    def _compute_all_custom_amounts(self):
        for move in self:
            amount_total = amount_subtotal = amount_tax = 0
            amount_subtotal_without_discount = amount_total_without_discount = amount_tax_without_discount = 0
            for line in move.invoice_line_ids:
                amount_subtotal += line.price_subtotal_wog_discount
                amount_total += line.price_total_wog_discount
                amount_tax += line.price_tax_wog_discount
                amount_subtotal_without_discount += line.price_subtotal_without_discount
                amount_total_without_discount += line.price_total_without_discount
                amount_tax_without_discount += line.price_tax_without_discount
            move.update({
                'amount_total_wog_discount': amount_total,
                'amount_subtotal_wog_discount': amount_subtotal,
                'amount_tax_wog_discount': amount_tax,
                'amount_total_without_discount': amount_total_without_discount,
                'amount_subtotal_without_discount': amount_subtotal_without_discount,
                'amount_tax_without_discount': amount_tax_without_discount,
            })

    @api.depends('amount_total', 'amount_total_without_discount')
    def _compute_discount_amounts(self):
        for move in self:
            move.discount_amount_total = move.amount_total_without_discount - move.amount_total
            move.discount_amount_subtotal = move.amount_subtotal_without_discount - move.amount_untaxed

    @api.constrains('discount_fixed', 'discount')
    def _check_global_discount_values(self):
        for move in self:
            if move.discount_type == "fixed" and (move.amount_total < move.discount_fixed or move.discount_fixed < 0):
                raise ValidationError(_("Discount amount must be greater than zero and less than total amount!"))
            if move.discount_type == "percentage" and (100 < move.discount or move.discount < 0):
                raise ValidationError(_("Discount (%) must be between 0 and 100!"))


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    discount_fixed = fields.Float(
        string='Discount', digits='Product Price', default=0.0
    )
    discount_percentage = fields.Float(
        string='Discount Percentage', digits='Discount', compute="_compute_discount_percentage"
    )
    # Global Discount
    discount_global = fields.Float(
        string="Discount Global", digits=(12, 9), default=0.0
    )
    discount_global_fixed = fields.Float(
        string="Discount Global Fixed", digits=(12, 7), default=0.0
    )
    price_subtotal_wog_discount = fields.Monetary(
        string='Subtotal without Global Discount',
        compute="_compute_custom_amount",
        help='Subtotal Without Global Discount'
    )
    price_total_wog_discount = fields.Monetary(
        string='Total without Global Discount',
        compute="_compute_custom_amount",
        help='Total Without Global Discount'
    )
    price_tax_wog_discount = fields.Monetary(
        string='Tax without Global Discount',
        compute="_compute_custom_amount",
        help='Tax Without Global Discount'
    )
    price_subtotal_without_discount = fields.Monetary(
        string='Subtotal without Discount',
        compute="_compute_custom_amount",
        help='Subtotal Without Discount'
    )
    price_total_without_discount = fields.Monetary(
        string='Total without Discount',
        compute="_compute_custom_amount",
        help='Total Without Discount'
    )
    price_tax_without_discount = fields.Monetary(
        string='Tax without Discount',
        compute="_compute_custom_amount",
        help='Tax Without Discount'
    )

    invoice_for_reconcile = fields.Many2one(comodel_name="account.move", string="Invoice for Reconcile")

    @api.depends('price_subtotal', 'price_subtotal_without_discount')
    def _compute_discount_percentage(self):
        for record in self:
            if record.price_subtotal and record.price_subtotal_without_discount:
                record.discount_percentage = 100 - (
                        record.price_subtotal / record.price_subtotal_without_discount * 100)
            else:
                record.discount_percentage = 0.0

    @api.onchange("quantity", "discount", "price_unit", "tax_ids", "discount_fixed")
    def _onchange_price_subtotal(self):
        return super(AccountMoveLine, self)._onchange_price_subtotal()

    @api.model
    def _get_price_total_and_subtotal_model(
            self,
            price_unit,
            quantity,
            discount,
            currency,
            product,
            partner,
            taxes,
            move_type,
    ):
        if self.discount_fixed or self.discount_global or self.discount_global_fixed:
            discount = self.get_discount()
        return super(AccountMoveLine, self)._get_price_total_and_subtotal_model(
            price_unit, quantity, discount, currency, product, partner, taxes, move_type
        )

    @api.model
    def _get_fields_onchange_balance_model(
            self,
            quantity,
            discount,
            amount_currency,
            move_type,
            currency,
            taxes,
            price_subtotal,
            force_computation=False,
    ):
        if self.discount_fixed or self.discount_global or self.discount_global_fixed:
            discount = self.get_discount()
        return super(AccountMoveLine, self)._get_fields_onchange_balance_model(
            quantity,
            discount,
            amount_currency,
            move_type,
            currency,
            taxes,
            price_subtotal,
            force_computation=force_computation,
        )

    def get_discount(self, discount=0.0):
        if discount:
            return discount
        if self.move_id.discount_type == "fixed":
            price_wo_discount = self.price_unit - self.discount_fixed - self.discount_global_fixed
        else:
            price_wo_discount = self.price_unit * (1 - (self.discount or 0.0) / 100)
            if self.discount_global:
                price_wo_discount = price_wo_discount * (1 - (self.discount_global or 0.0) / 100)
        if self.price_unit:
            discount = 100 - (price_wo_discount / self.price_unit * 100)
        return discount

    def reconcile(self):
        ''' Reconcile the current move lines all together.
        :return: A dictionary representing a summary of what has been done during the reconciliation:
                * partials:             A recorset of all account.partial.reconcile created during the reconciliation.
                * full_reconcile:       An account.full.reconcile record created when there is nothing left to reconcile
                                        in the involved lines.
                * tax_cash_basis_moves: An account.move recordset representing the tax cash basis journal entries.
        '''
        results = {}

        if not self:
            return results

        # List unpaid invoices
        not_paid_invoices = self.move_id.filtered(
            lambda move: move.is_invoice(include_receipts=True) and move.payment_state not in ('paid', 'in_payment')
        )

        # ==== Check the lines can be reconciled together ====
        company = None
        account = None
        for line in self:
            if line.reconciled:
                raise UserError(_("You are trying to reconcile some entries that are already reconciled."))
            if not line.account_id.reconcile and line.account_id.internal_type != 'liquidity':
                raise UserError(
                    _("Account %s does not allow reconciliation. First change the configuration of this account to allow it.")
                    % line.account_id.display_name)
            if line.move_id.state != 'posted':
                raise UserError(_('You can only reconcile posted entries.'))
            if company is None:
                company = line.company_id
            elif line.company_id != company:
                raise UserError(_("Entries doesn't belong to the same company: %s != %s")
                                % (company.display_name, line.company_id.display_name))
            if account is None:
                account = line.account_id
            elif line.account_id != account:
                raise UserError(_("Entries are not from the same account: %s != %s")
                                % (account.display_name, line.account_id.display_name))

        sorted_lines = self.sorted(key=lambda line: (line.date_maturity or line.date, line.currency_id))

        # ==== Collect all involved lines through the existing reconciliation ====

        involved_lines = sorted_lines
        involved_partials = self.env['account.partial.reconcile']
        current_lines = involved_lines
        current_partials = involved_partials
        while current_lines:
            current_partials = (current_lines.matched_debit_ids + current_lines.matched_credit_ids) - current_partials
            involved_partials += current_partials
            current_lines = (current_partials.debit_move_id + current_partials.credit_move_id) - current_lines
            involved_lines += current_lines

        # ==== Create partials ====

        partials = self.env['account.partial.reconcile'].create(sorted_lines._prepare_reconciliation_partials())

        # Track newly created partials.
        results['partials'] = partials
        involved_partials += partials

        # ==== Create entries for cash basis taxes ====

        is_cash_basis_needed = account.user_type_id.type in ('receivable', 'payable')
        if is_cash_basis_needed and not self._context.get('move_reverse_cancel'):
            tax_cash_basis_moves = partials._create_tax_cash_basis_moves()
            results['tax_cash_basis_moves'] = tax_cash_basis_moves

        # ==== Check if a full reconcile is needed ====

        if involved_lines[0].currency_id and all(
                line.currency_id == involved_lines[0].currency_id for line in involved_lines):
            is_full_needed = all(line.currency_id.is_zero(line.amount_residual_currency) for line in involved_lines)
        else:
            is_full_needed = all(line.company_currency_id.is_zero(line.amount_residual) for line in involved_lines)
        force_create_exchange_difference = self.env.context.get('force_create_exchange_difference', False)
        # create_exchange_difference_ = False
        # if force_create_exchange_difference:
        #     create_exchange_difference_ = all(
        #         line.currency_id.is_zero(line.amount_residual_currency) or line.company_currency_id.is_zero(
        #             line.amount_residual) for line in involved_lines)
        if is_full_needed or force_create_exchange_difference:

            # ==== Create the exchange difference move ====

            if self._context.get('no_exchange_difference'):
                exchange_move = None
            else:
                if force_create_exchange_difference:
                    exchange_move = involved_lines.filtered(lambda line: line.currency_id.is_zero(
                        line.amount_residual_currency) or line.company_currency_id.is_zero(
                        line.amount_residual))._create_exchange_difference_move()
                else:
                    exchange_move = involved_lines._create_exchange_difference_move()
                if exchange_move:
                    exchange_move_lines = exchange_move.line_ids.filtered(lambda line: line.account_id == account)

                    # Track newly created lines.
                    involved_lines += exchange_move_lines

                    # Track newly created partials.
                    exchange_diff_partials = exchange_move_lines.matched_debit_ids \
                                             + exchange_move_lines.matched_credit_ids
                    involved_partials += exchange_diff_partials
                    results['partials'] += exchange_diff_partials

                    exchange_move._post(soft=False)

            # ==== Create the full reconcile ====

            results['full_reconcile'] = self.env['account.full.reconcile'].create({
                'exchange_move_id': exchange_move and exchange_move.id,
                'partial_reconcile_ids': [(6, 0, involved_partials.ids)],
                'reconciled_line_ids': [(6, 0, involved_lines.ids)],
            })

        # Trigger action for paid invoices
        not_paid_invoices \
            .filtered(lambda move: move.payment_state in ('paid', 'in_payment')) \
            .action_invoice_paid()

        return results

    @api.model_create_multi
    def create(self, vals_list):
        prev_discount = {}
        for index, vals in enumerate(vals_list):
            price_unit = vals.get("price_unit")
            if vals.get("discount_fixed"):
                prev_discount[index] = {"discount_fixed": vals.get("discount_fixed"), "discount": 0.00}
                price_unit -= vals.get("discount_fixed")
                vals.update({"discount_fixed": 0.00})
            elif vals.get("discount"):
                price_unit = price_unit * (1 - vals.get("discount") / 100)
                prev_discount[index] = {"discount": vals.get("discount")}
            if vals.get("discount_global", False):
                price_unit = price_unit * (1 - vals.get("discount_global") / 100)
                if prev_discount.get(index, False):
                    prev_discount[index].update({"price_unit": vals.get("price_unit", 0.0),
                                                 "discount_global": vals.get("discount_global", 0.0),
                                                 "discount": vals.get("discount", 0.0)})
                else:
                    prev_discount[index] = {"price_unit": vals.get("price_unit", 0.0),
                                            "discount_global": vals.get("discount_global", 0.0),
                                            "discount": vals.get("discount", 0.0)}
                vals.update({"discount_global": 0.0})
            elif vals.get("discount_global_fixed"):
                price_unit = price_unit - vals.get("discount_global_fixed")
                if prev_discount.get(index, False):
                    prev_discount[index].update({"price_unit": vals.get("price_unit", 0.0),
                                                 "discount_global_fixed": vals.get("discount_global_fixed", 0.0),
                                                 "discount": vals.get("discount", 0.0)})
                else:
                    prev_discount[index] = {"price_unit": vals.get("price_unit", 0.0),
                                            "discount_global_fixed": vals.get("discount_global_fixed", 0.0),
                                            "discount": vals.get("discount", 0.0)}
                vals.update({"discount_global_fixed": 0.0})
            if price_unit and vals.get("price_unit", False):
                discount = 100 - (price_unit / vals.get("price_unit") * 100)
                vals.update({"discount": discount})
        res = super(AccountMoveLine, self).create(vals_list)
        i = 0
        for rec in res:
            if prev_discount.get(i, False):
                rec.write(prev_discount[i])
            i += 1
        return res

    def set_discount_to_zero(self, discount_type):
        for line in self:
            if discount_type == "fixed" and line.discount_fixed:
                line.discount_fixed = 0.0
            elif discount_type == "percentage" and line.discount:
                line.discount = 0.0

    @api.depends("quantity", "discount", "price_unit", "tax_ids", "discount_fixed", "exclude_from_invoice_tab")
    def _compute_custom_amount(self):
        for line in self:
            res = {}
            if not line.exclude_from_invoice_tab:
                price_unit_wog_discount = line._compute_price_reduce()
                price_unit_without_discount = line.price_unit
                subtotal = line.quantity * price_unit_wog_discount
                subtotal_without_discount = line.quantity * price_unit_wog_discount
                if line.tax_ids:
                    taxes_res = line.tax_ids._origin.compute_all(price_unit_wog_discount, quantity=line.quantity,
                                                                 currency=self.move_id.currency_id or self.env.company.currency_id,
                                                                 product=line.product_id,
                                                                 partner=self.move_id.partner_id)
                    taxes_res_without_discount = line.tax_ids._origin.compute_all(price_unit_without_discount,
                                                                                  quantity=line.quantity,
                                                                                  currency=self.move_id.currency_id or self.env.company.currency_id,
                                                                                  product=line.product_id,
                                                                                  partner=self.move_id.partner_id)
                    res['price_subtotal_wog_discount'] = taxes_res['total_excluded']
                    res['price_total_wog_discount'] = taxes_res['total_included']
                    res['price_tax_wog_discount'] = sum(t.get('amount', 0.0) for t in taxes_res.get('taxes', []))
                    res['price_subtotal_without_discount'] = taxes_res_without_discount['total_excluded']
                    res['price_total_without_discount'] = taxes_res_without_discount['total_included']
                    res['price_tax_without_discount'] = sum(
                        t.get('amount', 0.0) for t in taxes_res_without_discount.get('taxes', []))
                else:
                    res['price_total_wog_discount'] = res['price_subtotal_wog_discount'] = subtotal
                    res['price_subtotal_without_discount'] = res[
                        'price_total_without_discount'] = subtotal_without_discount
                    res['price_tax_wog_discount'] = 0.0
                    res['price_tax_without_discount'] = 0.0
                if line.currency_id:
                    res = {k: line.currency_id.round(v) for k, v in res.items()}
            else:
                res['price_total_wog_discount'] = res['price_subtotal_wog_discount'] = 0.0
                res['price_tax_wog_discount'] = 0.0
                res['price_subtotal_without_discount'] = res['price_total_without_discount'] = 0.0
                res['price_tax_without_discount'] = 0.0

            line.price_total_wog_discount = res["price_total_wog_discount"]
            line.price_subtotal_wog_discount = res["price_subtotal_wog_discount"]
            line.price_tax_wog_discount = res["price_tax_wog_discount"]
            line.price_subtotal_without_discount = res["price_subtotal_without_discount"]
            line.price_total_without_discount = res["price_total_without_discount"]
            line.price_tax_without_discount = res["price_tax_without_discount"]

    def _compute_price_reduce(self, global_discount=False):
        self.ensure_one()
        price_reduce = self.price_unit
        if self.move_id.discount_type == 'percentage':
            price_reduce = price_reduce * (1.0 - self.discount / 100.0)
            if global_discount:
                price_reduce = price_reduce * (1.0 - self.discount_global / 100.0)
        else:
            price_reduce -= self.discount_fixed
            if global_discount:
                price_reduce -= self.discount_global_fixed
        return price_reduce
