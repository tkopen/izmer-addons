from odoo import fields, models


class CustomerStatementWizard(models.TransientModel):
	_name = 'customer.statement.wizard'
	_description = 'Customer Statement Wizard'

	date_start = fields.Date(string="Date Start")
	date_end = fields.Date(string="Date End")
	partner_id = fields.Many2one(comodel_name="res.partner", string="Partner", required=True)

	def get_report(self):
		data = {'date_start': self.date_start, 'date_end': self.date_end, 'docids': self.partner_id.ids}
		return self.env.ref('nce_account.action_customer_statement_report').report_action(self, data=data)
