from datetime import datetime

from odoo import api, models, fields, _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT


class CustomerStatementReport(models.AbstractModel):
	_name = 'report.nce_account.customer_statement_report'
	_description = 'Customer Statement Report'

	def get_eur_currency(self):
		return self.env['res.currency'].search([('name', '=', 'EUR')], limit=1)

	@api.model
	def get_description(self, lines):
		description = ''
		line = False
		for line in lines:
			if line.payment_id:
				if line.matched_debit_ids:
					for matched_debit_id in line.matched_debit_ids:
						description += matched_debit_id.debit_move_id.move_id.name or ''
						description += ', '
			if line.move_id:
				if line.move_id.move_type == 'out_refund':
					description += line.move_id.name or ''
				else:
					description += line.move_id.partner_shipping_id.name or line.move_id.partner_id.name or ''
				description += ' '
		description += ' '
		if line:
			if line.payment_id:
				description += line.payment_id.ref or ''
				description += ' '
				description += str(line.payment_id.amount)
				description += line.payment_id.currency_id.symbol or ''
		return description

	def get_full_line_details(self, doc, date_start=False, date_end=False):
		payment_obj = self.env['account.payment']
		lines = []
		date_end_domain = ''
		if date_end:
			date_end_domain = f"AND am.date::date <= '{date_end}'"
		query = f"""SELECT aml.move_id, am.move_type, aml.currency_id, sum(aml.amount_currency) as amount_currency, am.date
	                FROM account_move_line aml
					LEFT JOIN account_move am ON am.id = aml.move_id
	                LEFT JOIN account_account ac ON aml.account_id = ac.id
	                WHERE aml.partner_id = {doc.id} {date_end_domain}
	                    AND am.state = 'posted'
	                    AND ac.internal_type = 'receivable'
	                    GROUP BY aml.move_id, am.move_type, aml.currency_id, am.date
	                    ORDER BY am.date"""
		self.env.cr.execute(query)
		records = self.env.cr.dictfetchall()
		move_obj = self.env['account.move']
		currency_obj = self.env['res.currency']
		lang_id = self.env['res.lang']._lang_get(self.env.context.get('lang') or 'en_US')
		date_format = lang_id.date_format
		totals = {currency_obj.browse(line['currency_id']): 0.0 for line in records if
				  line['move_type'] in ['out_invoice', 'out_refund']}
		for record in records:
			Move = move_obj.browse(record['move_id'])
			reconcile_lines = Move.line_ids.filtered(
				lambda line: line.account_id and line.account_id.internal_type == 'receivable')
			if Move.journal_id.type == 'general' and (Move.journal_id.code == 'EXCH' or Move.journal_id.code == 'KRFRK'):
				continue
			line_name = ''
			payment_ref = False
			date = False
			for move_line in reconcile_lines:
				currency = move_line.currency_id
				line_name += move_line.name or ''
				if move_line.payment_id:
					date = datetime.strptime(str(move_line.payment_id.date), DEFAULT_SERVER_DATE_FORMAT).strftime(
						date_format)
					payment_ref = move_line.payment_id.name

			record['move_name'] = payment_ref or Move.name
			payment_recs = payment_obj.search([('ref', '=', Move.name)])
			if payment_recs:
				record['move_name'] = payment_recs[0].name
			record['ref'] = Move.ref or Move.name
			record['name'] = line_name
			record['line_type'] = 'base'
			record['description'] = self.get_description(reconcile_lines)
			record['date'] = date or datetime.strptime(str(Move.date), DEFAULT_SERVER_DATE_FORMAT).strftime(
				date_format)
			if record.get('amount_currency') > 0.0:
				if Move.is_sale_document():
					record['debit'] = abs(record.get('amount_currency', 0.0))
					totals[currency] = totals.get(currency, 0.0) + record['debit']
					record['currency_id'] = currency
				else:
					for line in reconcile_lines:
						for match_line in line.matched_credit_ids:
							match_currency = match_line.credit_currency_id
							record['debit'] = record.get('debit', 0.0) + abs(match_line.credit_amount_currency)
							record['currency_id'] = match_currency
							totals[match_currency] = totals.get(match_currency, 0.0) + abs(
								match_line.credit_amount_currency)
			if record.get('amount_currency') < 0.0:
				if Move.is_sale_document():
					record['credit'] = abs(record['amount_currency'])
					totals[currency] = totals.get(currency, 0.0) - record['credit']
					record['currency_id'] = currency
				else:
					for line in reconcile_lines:
						for match_line in line.matched_debit_ids:
							match_currency = match_line.debit_currency_id
							record['credit'] = record.get('credit', 0.0) + abs(match_line.debit_amount_currency)
							record['currency_id'] = match_currency
							totals[match_currency] = totals.get(match_currency, 0.0) - abs(
								match_line.debit_amount_currency)
			record['total'] = {**totals}
			if not isinstance(record['currency_id'], int):
				lines.append(record)
		query = f"""SELECT aml.move_id, am.move_type, aml.currency_id,
								sum(aml.amount_residual_currency) as amount_currency, am.date
			                FROM account_move_line aml
							LEFT JOIN account_move am ON am.id = aml.move_id
			                LEFT JOIN account_account ac ON aml.account_id = ac.id
			                WHERE aml.partner_id = {doc.id} {date_end_domain}
			                    AND am.state = 'posted'
			                    AND ac.internal_type = 'receivable'
								AND aml.reconciled != true
								AND aml.amount_residual_currency != 0
								AND am.move_type = 'entry'
			                    GROUP BY aml.move_id, am.move_type, aml.currency_id, am.date
			                    ORDER BY am.date"""
		self.env.cr.execute(query)
		records = self.env.cr.dictfetchall()
		for record in records:
			move = move_obj.browse(record['move_id'])
			currency = currency_obj.browse(record['currency_id'])
			record['date'] = datetime.strptime(str(record['date']), DEFAULT_SERVER_DATE_FORMAT).strftime(date_format)
			record['ref'] = move.name
			record['line_type'] = 'base'
			record['description'] = ''
			record['currency_id'] = currency
			if record['amount_currency'] >= 0:
				record['debit'] = record['amount_currency']
			else:
				record['credit'] = abs(record['amount_currency'])
			totals[currency] = totals.get(currency, 0.0) - abs(record['amount_currency'])
			record['total'] = {**totals}
			lines.append(record)
		if date_start:
			date_start = fields.Date.to_date(date_start)
			second_lines = lines.copy()
			opening_line = {}
			for line in second_lines:
				line_date = datetime.strptime(line['date'], date_format).date()
				if line_date < date_start:
					opening_line[line['currency_id']] = line
					lines.pop(0)
			if opening_line:
				for key, line in opening_line.items():
					line['description'] = _('Opening Balance')
					line['date'] = date_start.strftime(date_format)
					line['line_type'] = 'opening'
					line['ref'] = ''
					if line['total'][line['currency_id']] > 0:
						line['debit'] = line['total'][line['currency_id']]
						line['credit'] = 0.0
					else:
						line['credit'] = abs(line['total'][line['currency_id']])
						line['debit'] = 0.0
					lines.insert(0, line)
		data = {'lines': lines, 'total': totals}
		return data

	@api.model
	def _get_report_values(self, docids, data=None):
		if data.get('docids', []):
			docids = data.get('docids', [])
		return {
			'doc_ids': docids,
			'doc_model': 'res.partner',
			'docs': self.env['res.partner'].browse(docids),
			'get_eur_currency': self.get_eur_currency,
			'get_full_line_details': self.get_full_line_details,
			'date_start': data.get('date_start', False),
			'date_end': data.get('date_end', False)
		}
