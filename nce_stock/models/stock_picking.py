# -*- coding: utf-8 -*-

from odoo import models


class StockPicking(models.Model):
	_inherit = 'stock.picking'

	def force_validate_button(self):
		for picking in self:
			for move in picking.move_ids_without_package:
				move.write({'quantity_done': move.product_uom_qty})
		return self.button_validate()
