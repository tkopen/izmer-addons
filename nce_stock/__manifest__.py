# -*- coding: utf-8 -*-
{
	'name': "NCE Stock Extended",
	'summary': """""",
	'description': """""",
	'author': "NCE Odoo",
	'website': "http://www.niyel-technologies.com/",
	'category': 'Inventory/Inventory',
	'version': '14.0.1.0',
	'depends': ['stock', 'nce_account'],
	'data': [
		# 'security/ir.model.access.csv',
		'views/templates.xml',
		'views/stock_picking_view.xml',
		'report/delivery_full_page.xml',
		'report/delivery_half_page.xml',
		'report/reports.xml',
	],
}
