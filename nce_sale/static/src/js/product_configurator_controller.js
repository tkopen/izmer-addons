odoo.define('nce_sale.ProductConfiguratorFormController', function (require) {
    "use strict";
    var ProductConfiguratorFormController = require('sale_product_configurator.ProductConfiguratorFormController');

    ProductConfiguratorFormController.include({
        _handleAdd: function () {
            var qty = this.$el.find('input[name="add_qty"]').val()
            this.$el.find('input[name="add_qty"]').val(qty.replace(',', '.'))
            this._super.apply(this, arguments);
        },
    });
});
