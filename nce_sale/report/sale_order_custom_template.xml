<odoo>
    <template id="sale_order_layout">
        <t t-if="not o" t-set="o" t-value="doc"/>
        <t t-if="not company">
            <!-- Multicompany -->
            <t t-if="company_id">
                <t t-set="company" t-value="company_id"/>
            </t>
            <t t-elif="o and 'company_id' in o and o.company_id.sudo()">
                <t t-set="company" t-value="o.company_id.sudo()"/>
            </t>
            <t t-else="else">
                <t t-set="company" t-value="res_company"/>
            </t>
        </t>
        <div class="header o_sale_order_layout">
            <div class="o_sale_order_header">
                <div class="row mb4">
                    <div class="col-4" style="font-family: sans-serif !important;">
                        <div name="company_address" style="font-size: 12px !important">
                            <span class="company_address" t-field="company.partner_id"
                                  t-options='{"widget": "contact", "fields": ["address", "name", "phone", "email", "website"]}'/>
                        </div>
                    </div>
                    <div class="col-3 offset-5" style="font-family: sans-serif !important;">
                        <img class="float-right" t-if="company.logo" t-att-src="image_data_uri(company.logo)"
                             alt="Logo"/>
                        <div t-if="company.social_twitter or company.social_facebook or company.social_youtube or company.social_instagram"
                             class="float-right social-media">
                            <span t-if="company.social_twitter" class="fa fa-twitter-square "/>&#160;<span
                                t-field="company.social_twitter"/>
                            <span t-if="company.social_facebook" class="fa fa-facebook-square "/>&#160;<span
                                t-field="company.social_facebook"/>
                            <span t-if="company.social_youtube" class="fa fa-youtube-square"/>&#160;<span
                                t-field="company.social_youtube"/>
                            <span t-if="company.social_instagram" class="fa fa-instagram"/>&#160;<span
                                t-field="company.social_instagram"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row not-first-page text-center" style="font-family: sans-serif !important;">
                <div class="col-4 text-center">
                    <strong t-field="doc.partner_id"/>
                </div>
                <div class="col-4">
                    <strong class="text-center">
                        <t t-if="env.context.get('proforma', False) or is_pro_forma">
                            Pro-Forma Invoice #:
                        </t>
                        <t t-else="">
                            <t t-if="doc.state not in ['draft','sent']">Order #:</t>
                            <t t-if="doc.state in ['draft','sent']">Quotation #:</t>
                        </t>
                        <span t-field="doc.name"/>
                    </strong>
                </div>
                <div class="col-4">
                    <strong class="text-center">Total:
                        <span t-field="doc.amount_total"/>
                    </strong>
                </div>
            </div>
        </div>
        <div class="article o_report_layout_sale_order"
             t-att-data-oe-model="o and o._name" t-att-data-oe-id="o and o.id"
             t-att-data-oe-lang="o and o.env.context.get('lang')">
            <t t-raw="0"/>
        </div>
        <div t-attf-class="footer o_sale_order_footer o_sale_order_layout">
            <div class="text-center"
                 style="border-top: 1px solid black; font-size: 12px !important; font-family: sans-serif !important;">
                <ul class="list-inline mb4">
                    <!-- using the list-inline-item class from bootstrap causes weird behaviours in pdf report
                         adding d-inline class fixes the problem-->
                    <li t-if="company.phone" class="list-inline-item d-inline">
                        Tel:
                        <span class="o_force_ltr" t-field="company.phone"/>
                    </li>
                    <li t-if="company.email" class="list-inline-item d-inline">
                        Email:
                        <span t-field="company.email"/>
                    </li>
                    <li t-if="company.website" class="list-inline-item d-inline">
                        Website:
                        <span t-field="company.website"/>
                    </li>
                    <li t-if="company.vat" class="list-inline-item d-inline"><t
                            t-esc="company.country_id.vat_label or 'Tax ID'"/>:
                        <span t-field="company.vat"/>
                    </li>
                </ul>

                <t t-if="company.report_footer">
                    <div name="financial_infos">
                        <span t-field="company.report_footer"/>
                    </div>
                </t>
                <div t-if="report_type == 'pdf'" class="text-muted">
                    Page:
                    <span class="page"/>
                    /
                    <span class="topage"/>
                </div>
            </div>
        </div>
    </template>

    <template id="sale_order_custom_tmpl_document">
        <t t-call="nce_sale.sale_order_layout">
            <t t-set="doc" t-value="doc.with_context({'lang':doc.partner_id.lang})"/>
            <t t-set="partner_lang" t-value="doc.partner_id.lang or company.partner_id.lang"/>
            <div class="page" style="font-family: sans-serif !important;">
                <div class="row">
                    <div class="col-4">
                        <strong t-if="doc.partner_shipping_id == doc.partner_invoice_id">Invoicing and Shipping
                            Address:
                        </strong>
                        <strong t-if="doc.partner_shipping_id != doc.partner_invoice_id">Invoicing Address:</strong>
                        <div class="partner_name">
                            <span t-field="doc.partner_invoice_id.name"/>
                        </div>
                        <div class="partner_address" t-field="doc.partner_invoice_id"
                             t-options='{"widget": "contact", "fields": ["address", "email", "phone"]}'/>
                    </div>
                    <div t-attf-class="{{ 'col-3' if env.context.get('proforma', False) or is_pro_forma else 'col-4'}}">
                        <t t-if="doc.partner_shipping_id != doc.partner_invoice_id">
                            <strong>Shipping Address:</strong>
                            <div class="partner_name">
                                <span t-field="doc.partner_shipping_id.name"/>
                            </div>
                            <div class="partner_address" t-field="doc.partner_shipping_id"
                                 t-options='{"widget": "contact", "fields": ["address", "email", "phone"]}'/>
                        </t>
                    </div>
                    <div t-attf-class="{{ 'col-5' if env.context.get('proforma', False) or is_pro_forma else 'col-4'}}">
                        <table class="table table_header">
                            <tr t-if="doc.name">
                                <td>
                                    <t t-if="env.context.get('proforma', False) or is_pro_forma">
                                        <strong>Pro-Forma Invoice #:</strong>
                                    </t>
                                    <t t-else="">
                                        <strong t-if="doc.state not in ['draft','sent']">Order #:</strong>
                                        <strong t-if="doc.state in ['draft','sent']">Quotation #:</strong>
                                    </t>
                                </td>
                                <td>
                                    <strong t-field="doc.name"/>
                                </td>
                            </tr>
                            <tr t-if="doc.date_order">
                                <td>
                                    <strong t-if="doc.state not in ['draft','sent']">Order Date:</strong>
                                    <strong t-if="doc.state in ['draft','sent']">Quotation Date:</strong>
                                </td>
                                <td>
                                    <span t-field="doc.date_order"
                                          t-options='{"widget": "date"}'/>
                                </td>
                            </tr>
                            <tr t-if="doc.validity_date and doc.state in ['draft','sent']">
                                <td>
                                    <strong>Valid Until:</strong>
                                </td>
                                <td>
                                    <span t-field="doc.validity_date"/>
                                </td>
                            </tr>
                            <tr t-if="doc.sudo().user_id.name">
                                <td>
                                    <strong>Salesperson:</strong>
                                </td>
                                <td>
                                    <span t-field="doc.sudo().user_id.name"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Total:</strong>
                                </td>
                                <td>
                                    <span t-field="doc.amount_total"/>
                                </td>
                            </tr>
                            <tr t-if="doc.client_order_ref">
                                <td>
                                    <strong>Client Ref:</strong>
                                </td>
                                <td>
                                    <span t-field="doc.client_order_ref"/>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div t-if="doc.project_title" class="row">
                    <div class="col-12 text-center">
                        <h5 class="project_title" t-field="doc.project_title"/>
                    </div>
                </div>
                <t t-set="display_discount" t-value="any(l.discount_percentage for l in doc.order_line)"/>
                <t t-set="display_image" t-value="any(l.product_id.image_1920 for l in doc.order_line)"/>
                <t t-set="display_tax" t-value="any(l.tax_id for l in doc.order_line)"/>
                <table class="table table-sm o_main_table">
                    <thead class="table_head">
                        <tr>
                            <th name="th_index" style="width: 6%">
                                <i class="fa fa-list-ol"/>
                                No
                            </th>
                            <t t-if="display_image">
                                <th name="th_image" style="width:80px;">
                                    <i class="fa fa-camera"/>
                                    Image
                                </th>
                            </t>
                            <th name="th_description" class="text-left">
                                <i class="fa fa-th-large"/>
                                Description
                            </th>
                            <th name="th_quantity" class="text-right">
                                <i class="fa fa-sort-amount-asc"/>
                                Quantity
                            </th>
                            <th name="th_priceunit" class="text-right">
                                <i class="fa fa-tags"/>
                                Unit Price
                            </th>
                            <th name="th_discount" t-if="display_discount" class="text-right"
                                groups="product.group_discount_per_so_line">
                                <i class="fa fa-minus-square"/>
                                Disc(%)
                            </th>
                            <th t-if="display_tax" name="th_taxes" class="text-right">
                                <i class="fa fa-pencil-square"/>
                                Taxes
                            </th>
                            <th name="th_subtotal" class="text-right">
                                <i class="fa fa-money"/>
                                <span groups="account.group_show_line_subtotals_tax_excluded">Subtotal</span>
                                <span groups="account.group_show_line_subtotals_tax_included">Total Price</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="table_body">
                        <t t-set="current_subtotal" t-value="0"/>
                        <t t-set="line_index_" t-value="1"/>

                        <t t-foreach="doc.order_line" t-as="line">

                            <t t-set="current_subtotal" t-value="current_subtotal + line.price_subtotal"
                               groups="account.group_show_line_subtotals_tax_excluded"/>
                            <t t-set="current_subtotal" t-value="current_subtotal + line.price_total"
                               groups="account.group_show_line_subtotals_tax_included"/>

                            <tr t-att-class="'bg-200 font-weight-bold o_line_section' if line.display_type == 'line_section' else 'font-italic o_line_note' if line.display_type == 'line_note' else ''">
                                <t t-if="not line.display_type">
                                    <td name="td_index">
                                        <span t-esc="line_index_"/>
                                    </td>
                                    <t t-set="line_index_" t-value="line_index_ + 1"/>
                                    <td t-if="display_image"
                                        name="image" class="text-center">
                                        <img t-attf-src="/web/image/product.product/#{line.product_id.id}/image_1920"
                                             style="max-width:60px;max-height:60px;"/>
                                    </td>
                                    <td name="td_name">
                                        <span t-field="line.name"/>
                                    </td>
                                    <td name="td_quantity" class="text-right">
                                        <span t-field="line.product_uom_qty"/>
                                        <span t-field="line.product_uom"/>
                                    </td>
                                    <td name="td_priceunit" class="text-right">
                                        <span t-field="line.price_unit"
                                              t-options='{"widget": "monetary", "display_currency": doc.pricelist_id.currency_id}'/>
                                    </td>
                                    <td t-if="display_discount" class="text-right"
                                        groups="product.group_discount_per_so_line">
                                        <span t-field="line.discount_percentage"/>
                                    </td>
                                    <td t-if="display_tax" name="td_taxes" class="text-right">
                                        <span t-esc="', '.join(map(lambda x: (x.description or x.name), line.tax_id))"/>
                                    </td>
                                    <td name="td_subtotal" class="text-right o_price_total">
                                        <span t-field="line.price_subtotal"
                                              groups="account.group_show_line_subtotals_tax_excluded"/>
                                        <span t-field="line.price_total"
                                              groups="account.group_show_line_subtotals_tax_included"/>
                                    </td>
                                </t>
                                <t t-if="line.display_type == 'line_section'">
                                    <td name="td_section_line" colspan="99">
                                        <span t-field="line.name"/>
                                    </td>
                                    <t t-set="current_section" t-value="line"/>
                                    <t t-set="current_subtotal" t-value="0"/>
                                </t>
                                <t t-if="line.display_type == 'line_note'">
                                    <td name="td_note_line" colspan="99">
                                        <span t-field="line.name"/>
                                    </td>
                                </t>
                            </tr>

                            <t t-if="current_section and (line_last or doc.order_line[line_index+1].display_type == 'line_section')">
                                <tr class="is-subtotal text-right">
                                    <td name="td_section_subtotal" colspan="99">
                                        <strong class="mr16">Subtotal</strong>
                                        <span
                                                t-esc="current_subtotal"
                                                t-options='{"widget": "monetary", "display_currency": doc.pricelist_id.currency_id}'
                                        />
                                    </td>
                                </tr>
                            </t>
                        </t>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-7">
                        <p t-if="doc.payment_term_id.note">
                            <span t-field="doc.payment_term_id.note"/>
                        </p>
                        <p>
                            <strong>Amount in words:</strong>
                            <span t-esc="doc.priceText()"/>
                        </p>
                    </div>
                    <div class="offset-1 col-4">
                        <table class="table table_total">
                            <t t-if="env.context.get('proforma', False) or is_pro_forma">
                                <tr t-if="doc.discount_amount_subtotal">
                                    <td>
                                        <strong>Gross Total:</strong>
                                    </td>
                                    <td class="text-right">
                                        <strong t-field="doc.amount_subtotal_without_discount"/>
                                    </td>
                                </tr>
                                <tr t-if="doc.discount_amount_subtotal">
                                    <td>
                                        <strong>Discount:</strong>
                                    </td>
                                    <td class="text-right">
                                        <span t-field="doc.discount_amount_subtotal"
                                              t-options='{"widget": "monetary", "display_currency": doc.pricelist_id.currency_id}'/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Net Total:</strong>
                                    </td>
                                    <td class="text-right">
                                        <strong t-field="doc.amount_untaxed"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Tax:</strong>
                                    </td>
                                    <td class="text-right">
                                        <strong t-field="doc.amount_tax"/>
                                    </td>
                                </tr>
                            </t>
                            <t t-else="">
                                <tr t-if="doc.discount_amount_total">
                                    <td>
                                        <strong>Gross Total:</strong>
                                    </td>
                                    <td class="text-right">
                                        <strong t-field="doc.amount_total_without_discount"/>
                                    </td>
                                </tr>
                                <tr t-if="doc.discount_amount_total">
                                    <td>
                                        <strong>Discount:</strong>
                                    </td>
                                    <td class="text-right">
                                        <span t-field="doc.discount_amount_total"
                                              t-options='{"widget": "monetary", "display_currency": doc.pricelist_id.currency_id}'/>
                                    </td>
                                </tr>
                            </t>
                            <tr>
                                <td>
                                    <strong>Total</strong>
                                </td>
                                <td class="text-right">
                                    <span t-field="doc.amount_total"/>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
                <p style="font-size: 12px !important" t-field="doc.note"/>
            </div>
        </t>
    </template>

    <template id="sale_order_custom_tmpl">
        <t t-call="web.html_container">
            <t t-foreach="docs" t-as="doc">
                <t t-call="nce_sale.sale_order_custom_tmpl_document" t-lang="doc.partner_id.lang"/>
            </t>
        </t>
    </template>

    <template id="sale_order_custom_tmpl_proforma">
        <t t-call="web.html_container">
            <t t-set="is_pro_forma" t-value="True"/>
            <t t-foreach="docs" t-as="doc">
                <t t-call="nce_sale.sale_order_custom_tmpl_document" t-lang="doc.partner_id.lang"/>
            </t>
        </t>
    </template>
</odoo>