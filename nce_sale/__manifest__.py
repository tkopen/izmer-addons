# -*- coding: utf-8 -*-

{
	'name': "NCE Sale Extended",
	'summary': """""",
	'description': """""",
	'author': "NCE Odoo",
	'website': "http://www.niyel-technologies.com/",
	'category': 'Sales/Sales',
	'version': '14.0.1.0',
	'depends': ['sale', 'nce_account', 'nce_mail_templates'],

	# always loaded
	'data': [
		# 'security/ir.model.access.csv',
		'data/report_paperformat.xml',
		'views/sale_order_view.xml',
		'views/assets.xml',
		'report/templates.xml',
		# 'report/sale_report_templates.xml',
		'report/sale_order_custom_template.xml',
		'report/reports.xml',
		'data/mail_data.xml',
	],
}
