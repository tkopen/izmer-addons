# -*- coding: utf-8 -*-
from functools import partial

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from odoo.tools import formatLang, float_compare

from odoo.addons.nce_account.models.tools import amount_to_words


class Sale(models.Model):
	_inherit = 'sale.order'

	@api.depends('order_line.price_total')
	def _amount_all(self):
		for order in self:
			max_tax_line = max_tax_amount = max_purchase_line = max_purchase_amount = amount_untaxed = amount_tax = 0.0
			for line in order.order_line:
				amount_untaxed += line.price_subtotal
				amount_tax += line.price_tax
				if line.price_total > max_purchase_amount:
					max_purchase_line = line
					max_purchase_amount = line.price_total
				if line.price_tax > max_tax_amount:
					max_tax_line = line
					max_tax_amount = line.price_tax
			amount_total = amount_untaxed + amount_tax
			if float_compare(order.amount_total_wog_discount - order.discount_fixed, order.amount_total,
							 precision_digits=3) != 0 and order.order_line:
				amount = order.amount_total_wog_discount - order.discount_fixed
				diff = round(amount_total - amount, 3)
				if max_tax_line:
					max_tax_line.update({'price_tax': max_tax_line.price_tax - diff,
										 'price_total': max_tax_line.price_total - diff})
					amount_tax -= diff
					amount_total = amount_tax + amount_untaxed
				else:
					max_purchase_line.update({'price_total': max_purchase_line.price_total - diff,
											  'price_subtotal': max_purchase_line.price_subtotal - diff})
					amount_total -= diff
					amount_untaxed -= diff
			order.update({
				'amount_untaxed': amount_untaxed,
				'amount_tax': amount_tax,
				'amount_total': amount_total,
			})

	@api.depends('order_line.price_total_wog_discount', 'order_line.price_total_without_discount')
	def _compute_all_custom_amounts(self):
		for order in self:
			amount_untaxed = amount_tax = 0.0
			amount_untaxed_without_discount = amount_tax_without_discount = 0.0
			for line in order.order_line:
				amount_untaxed += line.price_subtotal_wog_discount
				amount_tax += line.price_tax_wog_discount
				amount_untaxed_without_discount += line.price_subtotal_without_discount
				amount_tax_without_discount += line.price_tax_without_discount
			order.update({
				'amount_untaxed_wog_discount': amount_untaxed,
				'amount_tax_wog_discount': amount_tax,
				'amount_total_wog_discount': amount_untaxed + amount_tax,
				'amount_total_without_discount': amount_untaxed_without_discount + amount_tax_without_discount,
				'amount_subtotal_without_discount': amount_untaxed_without_discount,
				'amount_tax_without_discount': amount_tax_without_discount,
			})

	project_title = fields.Char(string="Title")
	pricelist_id = fields.Many2one(
		domain="[('changeable', '=', False), '|', ('company_id', '=', False), ('company_id', '=', company_id)]")
	discount_type = fields.Selection(
		string="Discount Type",
		selection=[('fixed', 'Fixed'),
				   ('percentage', 'Percentage')],
		required=True, default='percentage',
		states={'draft': [('readonly', False)],
				'sent': [('readonly', False)]}
	)
	discount_fixed = fields.Float(
		string='Discount', digits='Product Price',
		default=0.0, readonly=True,
		states={'draft': [('readonly', False)],
				'sent': [('readonly', False)]}
	)
	discount = fields.Float(
		string='Discount %', digits='Discount',
		default=0.0, readonly=True,
		states={'draft': [('readonly', False)],
				'sent': [('readonly', False)]}
	)
	discount_amount_subtotal = fields.Float(
		string='Discount Amount Subtotal', compute='_compute_discount_amounts'
	)
	discount_amount_total = fields.Float(
		string='Discount Amount Total', compute='_compute_discount_amounts'
	)
	amount_untaxed_wog_discount = fields.Monetary(
		string='Untaxed Amount without Global Discount',
		compute='_compute_all_custom_amounts',
		help="Untaxed Amount Without Global Discount",
	)
	amount_tax_wog_discount = fields.Monetary(
		string='Taxes without Global Discount',
		compute='_compute_all_custom_amounts',
		help="Taxes Without Global Discount",
	)
	amount_total_wog_discount = fields.Monetary(
		string='Total without Global Discount',
		compute='_compute_all_custom_amounts',
		help="Total Without Global Discount",
	)
	amount_total_without_discount = fields.Monetary(
		string='Total without Discount',
		compute='_compute_all_custom_amounts',
		help='Total Without Discount',
	)
	amount_subtotal_without_discount = fields.Monetary(
		string='Subtotal without Discount',
		compute='_compute_all_custom_amounts',
		help='Amount Subtotal Without Discount',
	)
	amount_tax_without_discount = fields.Monetary(
		string='Tax without Discount',
		compute='_compute_all_custom_amounts',
		help='Amount Tax Without Discount',
	)

	amount_by_group = fields.Binary(
		compute='_amount_by_group'
	)

	amount_undiscounted = fields.Float(compute='_compute_amount_undiscounted')

	def action_quotation_send(self):
		res = super(Sale, self).action_quotation_send()
		ctx = dict(res.get('context'))
		if ctx:
			ctx.update(
				dict(custom_layout="nce_mail_templates.mail_notification_paynow"))
			res.update(dict(context=ctx))
		return res

	def print_sale_order(self):
		return self.env.ref('nce_sale.action_sale_order_custom_template').report_action(self)

	def priceText(self):
		self.ensure_one()
		if self.partner_id.lang and self.partner_id.lang != 'tr_TR':
			return self.currency_id.amount_to_text(self.amount_total, lang_code=self.partner_id.lang)
		return amount_to_words(self.amount_total, self.currency_id)

	def _find_mail_template(self, force_confirmation_template=False):
		if self.state not in ['draft', 'sent']:
			return super(Sale, self)._find_mail_template(force_confirmation_template=force_confirmation_template)
		return self.env['ir.model.data'].xmlid_to_res_id('nce_sale.email_template_edi_sale', raise_if_not_found=False)

	@api.onchange("discount_type")
	def _onchange_discount_type(self):
		if self.discount_type and self.discount_type == 'percentage':
			self.discount_fixed = 0.0
			self.order_line.set_discount_to_zero("fixed")
		elif self.discount_type and self.discount_type == 'fixed':
			self.discount = 0.0
			self.order_line.set_discount_to_zero("percentage")

	def _amount_by_group(self):
		for order in self:
			currency = order.currency_id or order.company_id.currency_id
			fmt = partial(formatLang, self.with_context(lang=order.partner_id.lang).env, currency_obj=currency)
			res = {}
			for line in order.order_line:
				price_reduce = line._compute_price_reduce(global_discount=True)
				taxes = line.tax_id.compute_all(price_reduce, quantity=line.product_uom_qty, product=line.product_id,
												partner=order.partner_shipping_id)['taxes']
				for tax in line.tax_id:
					group = tax.tax_group_id
					res.setdefault(group, {'amount': 0.0, 'base': 0.0})
					for t in taxes:
						if t['id'] == tax.id or t['id'] in tax.children_tax_ids.ids:
							res[group]['amount'] += t['amount']
							res[group]['base'] += t['base']
			res = sorted(res.items(), key=lambda l: l[0].sequence)
			order.amount_by_group = [(
				l[0].name, l[1]['amount'], l[1]['base'],
				fmt(l[1]['amount']), fmt(l[1]['base']),
				len(res),
			) for l in res]

	@api.constrains('discount_fixed', 'discount')
	def _check_global_discount_values(self):
		for order in self:
			if order.discount_type == "fixed" and (
					order.amount_total_wog_discount < order.discount_fixed or order.discount_fixed < 0):
				raise ValidationError(_("Discount amount must be greater than zero and less than total amount!"))
			if order.discount_type == "percentage" and (100 < order.discount or order.discount < 0):
				raise ValidationError(_("Discount (%) must be between 0 and 100!"))

	def _compute_amount_undiscounted(self):
		for order in self:
			total = 0.0
			if order.discount_type == "percentage":
				for line in order.order_line:
					total += line.price_subtotal + line.price_unit * (
							(line.discount or 0.0) / 100.0) * line.product_uom_qty
				total += total / 100 * order.discount
			else:
				for line in order.order_line:
					total += line.price_subtotal + (line.discount_fixed * line.product_uom_qty)
				total += order.discount_fixed
			order.amount_undiscounted = total

	@api.depends('amount_total', 'amount_total_without_discount')
	def _compute_discount_amounts(self):
		for record in self:
			record.discount_amount_total = record.amount_total_without_discount - record.amount_total
			record.discount_amount_subtotal = record.amount_subtotal_without_discount - record.amount_untaxed

	def _prepare_invoice(self):
		res = super()._prepare_invoice()
		res.update({
			'discount_type': self.discount_type,
			'discount_fixed': self.discount_fixed,
			'discount': self.discount
		})
		return res


class SaleOrderLine(models.Model):
	_inherit = 'sale.order.line'

	discount_fixed = fields.Float(
		string="Discount",
		digits="Product Price",
		help="Fixed amount discount.",
	)
	discount_percentage = fields.Float(
		string='Discount Percentage', digits='Discount', compute="_compute_discount_percentage"
	)
	price_unit_net = fields.Float(
		string='Net Unit Price',
		compute='_compute_net_price', store=True
	)
	price_unit_net_with_tax = fields.Float(
		string='Net Unit Price (Tax Inc)',
		compute='_compute_net_price', store=True
	)

	# Global Discount
	discount_global = fields.Float(
		string="Discount Global", digits=(12, 9),
		compute="_compute_global_discount", store=True
	)
	discount_global_fixed = fields.Float(
		string="Discount Global Fixed", digits=(12, 7),
		compute="_compute_global_discount", store=True
	)
	price_subtotal_wog_discount = fields.Monetary(
		compute='_compute_custom_amount',
		string='Subtotal without Global Discount',
		help="Price Subtotal Without Global Discount",
	)
	price_tax_wog_discount = fields.Float(
		compute='_compute_custom_amount',
		string='Total Tax without Global Discount',
		help="Price Tax Without Global Discount",
	)
	price_total_wog_discount = fields.Monetary(
		compute='_compute_custom_amount',
		string='Total without Global Discount',
		help="Price Total Without Global Discount",
	)
	price_subtotal_without_discount = fields.Monetary(
		string='Subtotal without Discount',
		compute="_compute_custom_amount",
		help='Subtotal Without Discount'
	)
	price_total_without_discount = fields.Monetary(
		string='Total without Discount',
		compute="_compute_custom_amount",
		help='Total Without Discount'
	)
	price_tax_without_discount = fields.Monetary(
		string='Tax without Discount',
		compute="_compute_custom_amount",
		help='Tax Without Discount'
	)

	@api.depends('price_subtotal', 'price_subtotal_without_discount')
	def _compute_discount_percentage(self):
		for record in self:
			if record.price_subtotal and record.price_subtotal_without_discount:
				record.discount_percentage = 100 - (
						record.price_subtotal / record.price_subtotal_without_discount * 100)
			else:
				record.discount_percentage = 0.0

	@api.depends('product_uom_qty', 'discount', 'price_unit', 'tax_id',
				 'discount_fixed', 'discount_global', 'discount_global_fixed')
	def _compute_amount(self):
		for line in self:
			price = line._compute_price_reduce(True)
			taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty,
											product=line.product_id, partner=line.order_id.partner_shipping_id)
			line.update({
				'price_tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
				'price_total': taxes['total_included'],
				'price_subtotal': taxes['total_excluded'],
			})
			if self.env.context.get('import_file', False) and not self.env.user.user_has_groups(
					'account.group_account_manager'):
				line.tax_id.invalidate_cache(['invoice_repartition_line_ids'], [line.tax_id.id])

	@api.depends('order_id.amount_total_wog_discount', 'order_id.discount', 'order_id.discount_fixed')
	def _compute_global_discount(self):
		for line in self:
			order_id = line.order_id
			if order_id.discount_type and order_id.discount_type == "percentage" and order_id.discount:
				line.discount_global = order_id.discount
				line.discount_global_fixed = 0.0
			elif order_id.discount_type and order_id.discount_type == "fixed" and order_id.discount_fixed:
				line.discount_global_fixed = (line.price_unit - line.discount_fixed) * (
						order_id.discount_fixed / order_id.amount_total_wog_discount)
				line.discount_global = 0.0
			else:
				line.discount_global = 0.0
				line.discount_global_fixed = 0.0

	def set_discount_to_zero(self, discount_type):
		for line in self:
			if discount_type == "fixed" and line.discount_fixed:
				line.discount_fixed = 0.0
			elif discount_type == "percentage" and line.discount:
				line.discount = 0.0

	@api.depends('product_uom_qty', 'discount', 'price_unit', 'tax_id', 'discount_fixed')
	def _compute_custom_amount(self):
		for line in self:
			price = line._compute_price_reduce()
			price_without_discount = line.price_unit
			taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty,
											product=line.product_id, partner=line.order_id.partner_shipping_id)
			taxes_without_discount = line.tax_id.compute_all(price_without_discount, line.order_id.currency_id,
															 line.product_uom_qty, product=line.product_id,
															 partner=line.order_id.partner_shipping_id)
			line.update({
				'price_tax_wog_discount': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
				'price_total_wog_discount': taxes['total_included'],
				'price_subtotal_wog_discount': taxes['total_excluded'],
				'price_subtotal_without_discount': taxes_without_discount['total_excluded'],
				'price_total_without_discount': taxes_without_discount['total_included'],
				'price_tax_without_discount': sum(
					t.get('amount', 0.0) for t in taxes_without_discount.get('taxes', [])),
			})

	@api.depends('price_subtotal', 'price_total', 'product_uom')
	def _compute_net_price(self):
		for record in self:
			if record.product_id and record.product_uom and record.price_subtotal:
				product_uom_id = record.product_id.uom_id
				qty = record.product_uom_qty
				if record.product_uom != product_uom_id:
					qty = record.product_uom._compute_quantity(qty, product_uom_id)
				record.price_unit_net = record.price_subtotal / qty
				record.price_unit_net_with_tax = record.price_total / qty
			else:
				record.price_unit_net = 0.0
				record.price_unit_net_with_tax = 0.0

	def _compute_price_reduce(self, global_discount=False):
		self.ensure_one()
		price_reduce = self.price_unit
		if self.order_id.discount_type == 'percentage':
			price_reduce = price_reduce * (1.0 - self.discount / 100.0)
			if global_discount:
				price_reduce = price_reduce * (1.0 - self.discount_global / 100.0)
		else:
			price_reduce = price_reduce - self.discount_fixed
			if global_discount:
				price_reduce = price_reduce - self.discount_global_fixed
		return price_reduce

	@api.depends('price_unit', 'discount', 'discount_fixed')
	def _get_price_reduce(self):
		for line in self:
			line.price_reduce = line._compute_price_reduce()

	def _prepare_invoice_line(self, **optional_values):
		res = super()._prepare_invoice_line(**optional_values)
		res['discount_fixed'] = self.discount_fixed
		res['discount_global'] = self.discount_global
		res['discount_global_fixed'] = self.discount_global_fixed
		return res
