# -*- coding: utf-8 -*-
{
	'name': "Odoo ETL.",
	'summary': """Extract, transform, load""",
	'description': """In computing, extract, transform, load (ETL) is the general
	procedure of copying data from one or more sources into a destination system
	which represents the data differently from the source(s) or in a different context
	than the source(s).""",
	'author': "NCE Odoo",
	'website': "http://www.niyel-technologies.com/",
	'category': 'ETL',
	'version': '14.0.1.0',
	'depends': ['nce_product', 'nce_account'],
	'data': [
		'security/ir.model.access.csv',
		'views/views.xml',
		'views/templates.xml',
	],
}
