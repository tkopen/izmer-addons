from odoo import fields, models


# PARTNER TABLES
class PartnerCategory(models.Model):
    _inherit = 'res.partner.category'

    sync_id = fields.Integer(string="Sync Id")


class PartnerTitle(models.Model):
    _inherit = 'res.partner.title'

    sync_id = fields.Integer(string="Sync Id")


class Partner(models.Model):
    _inherit = 'res.partner'

    sync_id = fields.Integer(string="Sync Id")


class User(models.Model):
    _inherit = 'res.users'

    sync_id = fields.Integer(string="Sync Id")


# EMPLOYEE TABLES
class Department(models.Model):
    _inherit = 'hr.department'

    sync_id = fields.Integer(string="Sync Id")


class Employee(models.Model):
    _inherit = 'hr.employee'

    sync_id = fields.Integer(string="Sync Id")
