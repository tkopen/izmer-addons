# -*- coding: utf-8 -*-

from odoo import models, fields


class ProductAttribute(models.Model):
	_inherit = 'product.attribute'

	sync_id = fields.Integer(string="Sync Id")

	"""
	INSERT INTO product_attribute (sync_id, name, sequence, create_variant, display_type, create_uid, create_date, write_uid,
								   write_date)
	SELECT sync_id,
		   name,
		   COALESCE(sequence, 1) as sequence,
		   'always'              as create_variant,
		   display_type,
		   create_uid,
		   create_date,
		   write_uid,
		   write_date
	FROM dblink('dbname=odoo10_izmer user=dev1 password=joker200594 host=localhost',
				'select id,
					name,
					COALESCE(sequence, 1) as sequence,
					''always'' as create_variant,
					CASE WHEN name = ''Renk'' THEN ''color'' ELSE ''radio'' END display_type,
					1,
					create_date,
					1,
					write_date from product_attribute')
			 AS t1(sync_id integer,
				   name varchar,
				   sequence integer,
				   create_variant varchar,
				   display_type varchar,
				   create_uid integer,
				   create_date timestamp,
				   write_uid integer,
				   write_date timestamp)
	ON CONFLICT (sync_id) DO UPDATE SET name=excluded.name,
								   sequence=excluded.sequence,
								   create_variant=excluded.create_variant,
								   display_type=excluded.display_type,
								   create_uid=excluded.create_uid,
								   create_date=excluded.create_date,
								   write_uid=excluded.write_uid,
								   write_date=excluded.write_date;
	"""

	_sql_constraints = [
		('sync_uniq', 'unique (sync_id)', 'The sync_id must be unique!')
	]


class ProductAttributeValue(models.Model):
	_inherit = 'product.attribute.value'

	sync_id = fields.Integer(string="Sync Id")

	"""
	INSERT INTO product_attribute_value (sync_id, attribute_id, name, sequence, html_color, create_uid, create_date,
										 write_uid, write_date)
	SELECT sync_id,
		   (SELECT id from product_attribute where sync_id = attr_id) as attribute_id,
		   name,
		   sequence,
		   html_color,
		   create_uid,
		   create_date,
		   write_uid,
		   write_date
	FROM dblink('dbname=odoo10_izmer user=dev1 password=joker200594 host=localhost',
				'select id,
					attribute_id,
					name,
					COALESCE(sequence, 1) as sequence,
					CASE WHEN name = ''Siyah'' THEN ''#000000''
						WHEN name = ''Bej'' THEN ''#d5b791''
						WHEN name = ''Gri'' THEN ''#808080''
						WHEN name = ''Beyaz'' THEN ''#FFFFFF''
						WHEN name = ''Oksit Sarı'' THEN ''#f5a920''
						WHEN name = ''Jasmin'' THEN ''#F8DE7E''
						WHEN name = ''Beige'' THEN ''#F5F5DC''
						ELSE NULL END as html_color,
					1,
					create_date,
					1,
					write_date from product_attribute_value')
			 AS t1(sync_id integer,
				   attr_id integer,
				   name varchar,
				   sequence integer,
				   html_color varchar,
				   create_uid integer,
				   create_date timestamp,
				   write_uid integer,
				   write_date timestamp)
	ON CONFLICT (sync_id) DO UPDATE SET attribute_id=excluded.attribute_id,
										name=excluded.name,
										sequence=excluded.sequence,
										create_uid=excluded.create_uid,
										create_date=excluded.create_date,
										write_uid=excluded.write_uid,
										write_date=excluded.write_date;
	
	SELECT name, html_color
	FROM product_attribute_value;
	"""

	_sql_constraints = [
		('sync_uniq', 'unique (sync_id)', 'The sync_id must be unique!')
	]


class ProductCategory(models.Model):
	_inherit = 'product.category'

	sync_id = fields.Integer(string="Sync Id")

	_sql_constraints = [
		('sync_uniq', 'unique (sync_id)', 'The sync_id must be unique!')
	]


class ProductTemplate(models.Model):
	_inherit = 'product.template'

	sync_id = fields.Integer(string="Sync Id")

	_sql_constraints = [
		('sync_uniq', 'unique (sync_id)', 'The sync_id must be unique!')
	]


class Product(models.Model):
	_inherit = 'product.product'

	sync_id = fields.Integer(string="Sync Id")

	_sql_constraints = [
		('sync_uniq', 'unique (sync_id)', 'The sync_id must be unique!')
	]
