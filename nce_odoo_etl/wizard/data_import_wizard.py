from odoo import fields, models
from xmlrpc import client


class DataImportWizard(models.Model):
    _name = 'data.import.wizard'
    _description = 'Data Import Wizard'

    host = fields.Char(string="Host", required=True)
    port = fields.Char(string="Post", required=True)
    database_name = fields.Char(string="Database Name", required=True)
    username = fields.Char(string="Username", required=True)
    password = fields.Char(string="Password", required=True)

    def import_data(self, parent=False, update=False):
        record = self
        host = 'erp.izmer.com'
        port = '8069'
        database_name = 'IZMER_VERITABANI'
        username = 'info@izmer.com'
        password = 'Lila281216'
        if not record:
            record = self.search([('host', '=', host), ('port', '=', port), ('database_name', '=', database_name),
                                  ('username', '=', username), ('password', '=', password)], limit=1)
        if not record:
            record = self.create({
                'host': host,
                'port': port,
                'database_name': database_name,
                'username': username,
                'password': password,
            })
        record.import_partner_tables(parent)
        self.env.cr.commit()
        if parent:
            record.import_employee_tables(update)
        return True

    def import_partner_tables(self, parent):
        url = f'http://{self.host}:{self.port}'
        common = client.ServerProxy('{}/xmlrpc/2/common'.format(url))
        uid = common.authenticate(self.database_name, self.username, self.password, {})
        objects = client.ServerProxy('{}/xmlrpc/2/object'.format(url))
        category_fields = ['id', 'name']
        old_categories = objects.execute_kw(self.database_name, uid, self.password,
                                            'res.partner.category', 'search_read',
                                            [],
                                            {'fields': category_fields})
        existing_category_dict = {line.sync_id: line for line in self.env['res.partner.category'].search([])}
        if old_categories:
            self.env['res.partner.category'].create([{'name': line['name'],
                                                      'sync_id': line['id']} for line in
                                                     old_categories if
                                                     not existing_category_dict.get(line['id'],
                                                                                    False)])
        old_titles = objects.execute_kw(self.database_name, uid, self.password,
                                        'res.partner.title', 'search_read', [], {'fields': category_fields})
        existing_title_dict_id = {line.id: line for line in
                                  self.env['res.partner.title'].search([('sync_id', '=', False)])}
        existing_title_dict_sync_id = {line.sync_id: line for line in
                                       self.env['res.partner.title'].search([('sync_id', '!=', False)])}
        titles_to_create = []
        for title in old_titles:
            if existing_title_dict_sync_id.get(title['id'], False):
                continue
            elif existing_title_dict_id.get(title['id'], False):
                existing_title_dict_id[title['id']].write({'sync_id': title['id']})
            else:
                titles_to_create.append({
                    'name': title['name'],
                    'sync_id': title['id'],
                })
        if titles_to_create:
            self.env['res.partner.title'].create(titles_to_create)
        partner_fields = [
            'id',
            'category_id',
            'name',
            'comment',
            'website',
            'color',
            'active',
            'street',
            'city',
            'zip',
            'title',
            'country_id',
            'parent_id',
            'company_name',
            'employee',
            'ref',
            'email',
            'is_company',
            'function',
            'street2',
            'phone',
            'date',
            'tz',
            'credit_limit',
            'mobile',
            'type',
            'vat',
            'state_id',
            'message_bounce',
            'signup_type',
            'signup_expiration',
            'signup_token',
            'picking_warn_msg',
            'picking_warn',
            'debit_limit',
            'invoice_warn_msg',
            'invoice_warn',
            'sale_warn',
            'sale_warn_msg',
            'purchase_warn',
            'purchase_warn_msg',
        ]
        partner_domain = []
        if not parent:
            partner_domain.append(['parent_id', '=', False])
        partner_domain += ['|', ['active', '=', False], ['active', '=', True]]

        old_partners = objects.execute_kw(self.database_name, uid, self.password, 'res.partner', 'search_read',
                                          [partner_domain],
                                          {'fields': partner_fields})
        existing_partner_dict_sync_id = {line.sync_id: line for line in
                                         self.env['res.partner'].with_context(active_test=False).search([])}
        existing_partner_dict_id = {line.id: line for line in
                                    self.env['res.partner'].with_context(active_test=False).search([])}
        partners_to_create = []
        for partner in old_partners:
            try:
                if partner['id'] == 1:
                    values = self.prepare_partner_values(partner, existing_partner_dict_sync_id, parent)
                    existing_partner_dict_id[1].write(values)
                elif partner['id'] == 3:
                    values = self.prepare_partner_values(partner, existing_partner_dict_sync_id, parent)
                    existing_partner_dict_id[3].write(values)
                elif partner['id'] == 4:
                    values = self.prepare_partner_values(partner, existing_partner_dict_sync_id, parent)
                    existing_partner_dict_id[5].write(values)
                elif partner['id'] == 5:
                    values = self.prepare_partner_values(partner, existing_partner_dict_sync_id, parent)
                    existing_partner_dict_id[4].write(values)
                elif partner['id'] == 6:
                    values = self.prepare_partner_values(partner, existing_partner_dict_sync_id, parent)
                    existing_partner_dict_id[6].write(values)
                elif not existing_partner_dict_sync_id.get(partner['id'], False) and partner['id'] not in [1, 2, 3, 4,
                                                                                                           5, 6]:
                    partners_to_create.append(
                        self.prepare_partner_values(partner, existing_partner_dict_sync_id))
            except:
                a = 45
        if partners_to_create:
            self.env['res.partner'].create(partners_to_create)
        if not parent:
            return True
        partner_dict = {line.sync_id: line for line in self.env['res.partner'].search([])}

        user_fields = [
            'id',
            'active',
            'login',
            'password',
            'partner_id',
            'signature',
            'target_sales_done',
            'target_sales_won',
            'target_sales_invoiced',
        ]
        old_users = objects.execute_kw(self.database_name, uid, self.password, 'res.users', 'search_read',
                                       [['|', ['active', '=', False], ['active', '=', True]]], {'fields': user_fields})
        existing_user_dict_sync_id = {line.sync_id: line for line in
                                      self.env['res.users'].with_context(active_test=False).search([])}
        existing_user_dict_id = {line.id: line for line in
                                 self.env['res.users'].with_context(active_test=False).search([])}
        users_to_create = []
        for user in old_users:
            try:
                if existing_user_dict_sync_id.get(user['id'], False):
                    continue
                elif user['id'] == 1:
                    values = self.prepare_partner_values(user, partner_dict)
                    existing_user_dict_id[1].write(values)
                elif user['id'] == 3:
                    values = self.prepare_partner_values(user, partner_dict)
                    existing_user_dict_id[3].write(values)
                elif user['id'] == 4:
                    values = self.prepare_partner_values(user, partner_dict)
                    existing_user_dict_id[4].write(values)
                elif user['id'] == 5:
                    values = self.prepare_partner_values(user, partner_dict)
                    existing_user_dict_id[5].write(values)
                elif not existing_user_dict_sync_id.get(user['id'], False):
                    users_to_create.append(self.prepare_user_values(user, partner_dict))
            except:
                a = 45
        if users_to_create:
            self.env['res.users'].with_context(install_mode=True).create(users_to_create)
        return True

    def prepare_user_values(self, user, partner_dict):
        return {
            'sync_id': user['id'],
            'active': user['active'],
            'login': user['login'],
            'password': user['password'],
            'partner_id': partner_dict[user['partner_id'][0]].id,
            'signature': user['signature'],
            'target_sales_done': user['target_sales_done'],
            'target_sales_won': user['target_sales_won'],
            'target_sales_invoiced': user['target_sales_invoiced'],
        }

    def prepare_partner_values(self, partner, existing_partner_dict_sync_id):
        try:
            values = {
                'sync_id': partner['id'],
                'name': partner['name'],
                'comment': partner['comment'],
                'website': partner['website'],
                'color': partner['color'],
                'active': partner['active'],
                'street': partner['street'],
                'city': partner['city'],
                'zip': partner['zip'],
                'title': partner['title'][0] if partner['title'] else False,
                'country_id': partner['country_id'][0] if partner['country_id'] else False,
                'parent_id': existing_partner_dict_sync_id[partner['parent_id'][0]].id if partner[
                                                                                              'parent_id'] and existing_partner_dict_sync_id.get(
                    partner['parent_id'][0], False) else False,
                'company_name': partner['company_name'],
                'employee': partner['employee'],
                'ref': partner['ref'],
                'email': partner['email'],
                'is_company': partner['is_company'],
                'function': partner['function'],
                'street2': partner['street2'],
                'phone': partner['phone'],
                'date': partner['date'],
                'tz': partner['tz'],
                'credit_limit': partner['credit_limit'],
                'mobile': partner['mobile'],
                'type': partner['type'],
                'vat': partner['vat'],
                'state_id': partner['state_id'][0] if partner['state_id'] else False,
                'message_bounce': partner['message_bounce'],
                'signup_type': partner['signup_type'],
                'signup_expiration': partner['signup_expiration'],
                'signup_token': partner['signup_token'],
                'picking_warn_msg': partner['picking_warn_msg'],
                'picking_warn': partner['picking_warn'],
                'debit_limit': partner['debit_limit'],
                'invoice_warn_msg': partner['invoice_warn_msg'],
                'invoice_warn': partner['invoice_warn'],
                'sale_warn': partner['sale_warn'],
                'sale_warn_msg': partner['sale_warn_msg'],
                'purchase_warn': partner['purchase_warn'],
                'purchase_warn_msg': partner['purchase_warn_msg'],
            }
        except:
            a = 23
        return values

    def import_employee_tables(self, update):
        url = f'http://{self.host}:{self.port}'
        common = client.ServerProxy('{}/xmlrpc/2/common'.format(url))
        uid = common.authenticate(self.database_name, self.username, self.password, {})
        objects = client.ServerProxy('{}/xmlrpc/2/object'.format(url))

        old_departments = objects.execute_kw(self.database_name, uid, self.password, 'hr.department', 'search_read',
                                             [],
                                             {'fields': ['name', 'id']})
        existing_department_dict_sync_id = {line.sync_id: line for line in self.env['hr.department'].search([])}
        existing_department_dict_id = {line.id: line for line in self.env['hr.department'].search([])}
        departments_to_create = []
        for department in old_departments:
            try:
                if department['id'] == 1:
                    existing_department_dict_id[1].write({'name': department['name'],
                                                          'sync_id': department['id']})
                elif department['id'] == 2:
                    existing_department_dict_id[2].write({'name': department['name'],
                                                          'sync_id': department['id']})
                elif not existing_department_dict_sync_id.get(department['id'], False):
                    departments_to_create.append({'name': department['name'],
                                                  'sync_id': department['id']})
            except:
                a = 45
        if departments_to_create:
            self.env['hr.department'].create(departments_to_create)
        employee_fields = [
            'id',
            'active',
            'address_id',
            'marital',
            'identification_id',
            'parent_id',
            'work_phone',
            'user_id',
            'resource_id',
            'country_id',
            'department_id',
            'mobile_phone',
            'birthday',
            'sinid',
            'work_email',
            'work_location',
            'gender',
            'name_related',
            'notes',
            'address_home_id',
            'passport_id',
            'children',
            'vehicle',
            'place_of_birth',
            'salary_type',
            'image'
        ]
        old_employees = objects.execute_kw(self.database_name, uid, self.password,
                                           'hr.employee', 'search_read',
                                           [['|', ['active', '=', False], ['active', '=', True]]],
                                           {'fields': employee_fields})

        existing_employee_dict_sync_id = {line.sync_id: line for line in
                                          self.env['hr.employee'].with_context(active_test=False).search([])}
        employee_to_create = []
        users_dict = {line.sync_id: line for line in self.env['res.users'].with_context(active_test=False).search([])}
        partner_dict = {line.sync_id: line for line in
                        self.env['res.partner'].with_context(active_test=False).search([])}
        country_dict = {line.id: line for line in self.env['res.country'].search([])}
        department_dict = {line.sync_id: line for line in self.env['hr.department'].search([])}
        for employee in old_employees:
            if not existing_employee_dict_sync_id.get(employee['id'], False):
                values = self.prepare_employee_values(employee, users_dict, partner_dict,
                                                      department_dict, country_dict,
                                                      existing_employee_dict_sync_id)
                if values:
                    employee_to_create.append(values)
            if existing_employee_dict_sync_id.get(employee['id'], False) and update:
                existing_employee_dict_sync_id[employee['id']].write(
                    self.prepare_employee_values(employee, users_dict, partner_dict,
                                                 department_dict, country_dict,
                                                 existing_employee_dict_sync_id)
                )
        if employee_to_create:
            self.env['hr.employee'].create(employee_to_create)
        return True

    def prepare_employee_values(self, employee, users_dict, partner_dict, department_dict, country_dict,
                                existing_employee_dict_sync_id):
        if employee['parent_id'] and not existing_employee_dict_sync_id.get(employee['parent_id'][0], False):
            return False
        user_id = users_dict[employee['user_id'][0]].id if employee['user_id'] and users_dict.get(
            employee['user_id'][0], False) else False
        address_home_id = partner_dict[employee['address_home_id'][0]].id if employee['address_home_id'] and \
                                                                             employee['address_home_id'][
                                                                                 0] else False
        department_id = department_dict[employee['department_id'][0]].id if employee['department_id'] and \
                                                                            employee['department_id'][0] else False
        address_id = partner_dict[employee['address_id'][0]].id if employee['address_id'] and \
                                                                   employee['address_id'][0] else False
        country_id = country_dict[employee['country_id'][0]].id if employee['country_id'] and \
                                                                   employee['country_id'][0] else False
        values = {
            'sync_id': employee['id'],
            'address_id': address_id,
            'marital': employee['marital'],
            'identification_id': employee['identification_id'],
            'parent_id': existing_employee_dict_sync_id[employee['parent_id'][0]].id if employee[
                'parent_id'] else False,
            'work_phone': employee['work_phone'],
            # 'resource_id': country_dict[employee['resource_id'][0]].id if employee['resource_id'] else False,
            'country_id': country_id,
            'department_id': department_id,
            'mobile_phone': employee['mobile_phone'],
            'birthday': employee['birthday'],
            'sinid': employee['sinid'],
            'work_email': employee['work_email'],
            'work_location': employee['work_location'],
            'gender': employee['gender'],
            'active': employee['active'],
            'name': employee['name_related'] or 'New Name',
            'notes': employee['notes'],
            'address_home_id': address_home_id,
            'passport_id': employee['passport_id'],
            'children': employee['children'],
            'vehicle': employee['vehicle'],
            'place_of_birth': employee['place_of_birth'],
            'salary_type': employee['salary_type'] or 'monthly',
            'image_1920': employee['image'],
            'user_id': user_id
        }
        return values
