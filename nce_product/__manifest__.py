# -*- coding: utf-8 -*-
{
	'name': "NCE Product Extended",
	'summary': """""",
	'description': """""",
	'author': "Niyel Technologies",
	'website': "http://www.niyel-technologies.com/",
	'category': 'Sales/Sales',
	'version': '14.0.1.0',
	'depends': ['product'],
	'data': [
		'security/ir.model.access.csv',
		'views/product_view.xml',
		'views/product_pricelist_view.xml',
		'wizard/product_pricelist_create_wizard_view.xml',
	],
	'qweb': [
		"static/src/xml/base.xml",
	],
}
