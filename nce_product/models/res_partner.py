from odoo import models, fields


class Partner(models.Model):
    _inherit = 'res.partner'

    property_product_pricelist = fields.Many2one(comodel_name="product.pricelist", domain=[('changeable', '=', False)])
