# -*- coding: utf-8 -*-

from odoo import models, fields


class ProductPricelist(models.Model):
    _inherit = 'product.pricelist'

    changeable = fields.Boolean(string="Changeable", default=False)
    pricelist_type = fields.Selection(string="Pricelist", required=True, default="other",
                                      selection=[('retail', 'Retail'),
                                                 ('other', 'Other')])


class ProductPricelistItem(models.Model):
    _inherit = 'product.pricelist.item'

    def _default_pricelist_id(self):
        return self.env['product.pricelist'].search([('changeable', '=', True), ('pricelist_type', '=', 'retail'),
                                                     '|', ('company_id', '=', False),
                                                     ('company_id', '=', self.env.company.id)], limit=1)

    pricelist_id = fields.Many2one(default=_default_pricelist_id)
    changeable = fields.Boolean(string="Changeable", related="pricelist_id.changeable")
