from odoo import fields, models


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    sale_price = fields.Float(string="Sale Price", related="product_variant_id.sale_price")
    sale_price_currency_id = fields.Many2one(comodel_name="res.currency",
                                             string="Sale Price Currency",
                                             related="product_variant_id.sale_price_currency_id")


class Product(models.Model):
    _inherit = 'product.product'

    sale_price = fields.Float(string="Sale Price", compute="_compute_sale_price")
    sale_price_currency_id = fields.Many2one(comodel_name="res.currency", string="Sale Price Currency",
                                             compute="_compute_sale_price")

    def _compute_sale_price(self):
        prices = self.env['product.pricelist.item'].search([('pricelist_id.changeable', '=', 'True'),
                                                            ('product_id', 'in', self.ids),
                                                            ('pricelist_id.pricelist_type', '=', 'retail'),
                                                            ('applied_on', '=', '0_product_variant')])

        prices_map = {line.product_id.id: line for line in prices}
        for product in self:
            product_price = prices_map.get(product.id, )
            product.sale_price = product_price and product_price.fixed_price or 0.0
            product.sale_price_currency_id = product_price and product_price.pricelist_id.currency_id or False
