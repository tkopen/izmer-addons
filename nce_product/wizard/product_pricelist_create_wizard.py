from odoo import models, fields


class ProductPricelistCreateWizard(models.TransientModel):
    _name = 'product.pricelist.create.wizard'
    _description = 'Product Pricelist Create Wizard'

    product_tmpl_id = fields.Many2one(comodel_name="product.template", string="Product Template",
                                      required=True, readonly=True)
    pricelist_id = fields.Many2one(comodel_name="product.pricelist", required=True,
                                   domain="[('changeable', '=', True)]")

    def create_pricelists(self):
        self.ensure_one()
        if not self.product_tmpl_id:
            return False
        pricelist_items_to_create = []

        pricelist_map = {line.product_id.id: True for line in self.env['product.pricelist.item'].search(
            [('pricelist_id', '=', self.pricelist_id.id),
             ('product_id', 'in', self.product_tmpl_id.product_variant_ids.ids),
             ('applied_on', '=', '0_product_variant')])}
        for product in self.product_tmpl_id.product_variant_ids:
            if not pricelist_map.get(product.id, False):
                pricelist_items_to_create.append({
                    'applied_on': '0_product_variant',
                    'pricelist_id': self.pricelist_id.id,
                    'product_tmpl_id': self.product_tmpl_id.id,
                    'product_id': product.id,
                    'compute_price': 'fixed',
                    'fixed_price': 0.0
                })
        if pricelist_items_to_create:
            self.env['product.pricelist.item'].create(pricelist_items_to_create)
        return True
